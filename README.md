<img class="top-banner" src="assets/img/top-banner.png" width="780" height="204" alt="アリスとボブのバナー">

# アリスとボブ {#top}

バージョン： 2.4

アリスとボブはやりとりをしたいが、
デジタル技術が広く行き渡っている情報化時代の中に生きていて、
蔓延している監視と統制から自身と相手を守りたい。
どうしたらいいか？

## 目的 {#purpose}

本シリーズは、デジタルの世界において
自身とみんなのプライバシー、安全と自由を守りたい人たち向けに
作成したデジタルセキュリティ対策の一連のガイドです。
特に、以下の目的を意識しながら作成しようとしています。

- 社会におけるデジタルセキュリティの願望と実践の溝埋め、
  そしてセキュリティ水準の引き上げ。
- よりよい世界に向けて活動している市民社会の心身と財産と活動の保護。
- デジタル技術利用者の技術的支配下からの解放、
  そして自由技術への移行の支援。
- プライバシー、心身と財産の安全、思想と良心と表現の自由、
  技術的自由などの尊厳と権利の擁護。

## ガイド一覧 {#list}

- **序文** \
  --- [ここで読む（Markdown）](ja-md/alice2bob-ja-preface-2.4.md)
  \| [HTML](ja-html/alice2bob-ja-preface-2.4.html)
  \
  （最終更新： 2022-08-12）
- **デジタルセキュリティの概念** \
  --- [ここで読む（Markdown）](ja-md/alice2bob-ja-security-concepts-2.4.md)
  \| [HTML](ja-html/alice2bob-ja-security-concepts-2.4.html)
  \
  （最終更新： 2023-08-17）
- **セキュリティ計画** \
  --- [ここで読む（Markdown）](ja-md/alice2bob-ja-security-plan-2.4.md)
  \| [HTML](ja-html/alice2bob-ja-security-plan-2.4.html)
  \
  （最終更新： 2021-12-29 （原文更新： 2021-02-02））
- **Tor 匿名化ネットワーク** \
  --- [ここで読む（Markdown）](ja-md/alice2bob-ja-Tor-2.4.md)
  \| [HTML](ja-html/alice2bob-ja-Tor-2.4.html)
  \
  （最終更新： 2024-02-05）
- **Tor Browser** \
  --- [ここで読む（Markdown）](ja-md/alice2bob-ja-Tor-Browser-2.4.md)
  \| [HTML](ja-html/alice2bob-ja-Tor-Browser-2.4.html)
  \
  （最終更新： 2024-08-20）
- **Tails を用いた自由かつ匿名コンピューティング** \
  --- [ここで読む（Markdown）](ja-md/alice2bob-ja-Tails-2.4.md)
  \| [HTML](ja-html/alice2bob-ja-Tails-2.4.html)
  \
  （最終更新： 2024-08-16）
- **暗号技術入門** \
  --- [ここで読む（Markdown）](ja-md/alice2bob-ja-intro-cryptography-2.4.md)
  \| [HTML](ja-html/alice2bob-ja-intro-cryptography-2.4.html)
  \
  （最終更新： 2022-08-11）
- **OpenPGP と GnuPG** \
  --- [ここで読む（Markdown）](ja-md/alice2bob-ja-OpenPGP-2.4.md)
  \| [HTML](ja-html/alice2bob-ja-OpenPGP-2.4.html)
  \
  （最終更新： 2023-08-17）
- **用語集** \
  --- [ここで読む（Markdown）](ja-md/alice2bob-ja-glossary-2.4.md)
  \| [HTML](ja-html/alice2bob-ja-glossary-2.4.html)
  \
  （最終更新： 2022-02-28）
- **外部資料** \
  --- [ここで読む（Markdown）](ja-md/alice2bob-ja-external-resources-2.4.md)
  \| [HTML](ja-html/alice2bob-ja-external-resources-2.4.html)
  \
  （最終更新： 2024-08-20）

以下のいずれかの手順で全てのガイドを一括ダウンロードできます。

- URL `https://git.disroot.org/alice2bob/alice2bob` →
  git リポジトリ URL （ファイル一覧の上）の右のダウンロードボタン →
  "ZIP" または "TAR GZ"
- git リポジトリ URL `https://git.disroot.org/alice2bob/alice2bob.git`
  をコピーして、 git クローンをする。

## 連絡先 {#contact}

コメントや建設的批判があったら是非お聞かせください。

### PGP 鍵 {#contact-PGP}

`1D3E 313C 4DB2 B3E0 8271 FC48 89BB 4CBB 9DBE 7F6D`

- [ウェブサイト上](alice2bob.asc)
- 鍵サーバ（クリアネット）：
  `https://keys.openpgp.org/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D`
- 鍵サーバ（オニオン）：
  `http://zkaan2xfbuxia2wpf7ofnkbz6r5zdbbvxbunvp5g2iebopbfc4iqmbad.onion/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D`

### ウェブサイト {#contact-website}

`https://git.disroot.org/alice2bob/alice2bob`

## 検証 {#verify-alice2bob}

[検証について](verify.md)

- [SHA256 の PGP 署名](alice2bob-SHA256-2.4.sig)
- [SHA256 チェックサム](alice2bob-SHA256-2.4)

## 免責事項 {#disclaimer}

アリスとボブはセキュリティ専門家でも法律家でも
金融アドバイザーでも医師でもありません。
本シリーズの目的はあくまでも情報提供で、何の助言でもありません。
本シリーズは全くの無保証で提供されます。
本シリーズによってどんな不利、損失または損害が生じても、
アリスとボブは一切の責任を負いません。
ご自身の状況を考慮し、自己責任で本シリーズを使ってください。
