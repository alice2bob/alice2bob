# 検証 {#top}

## PGP 鍵とチェックサム {#PGP-and-checksums}

アリスとボブの全ての内容とチェックサムは
下記の PGP 鍵で保護されています。

- PGP 鍵： `1D3E 313C 4DB2 B3E0 8271 FC48 89BB 4CBB 9DBE 7F6D`
  - [ウェブサイト上](alice2bob.asc)
  - [鍵サーバ（クリアネット）](https://keys.openpgp.org/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D)
  - [鍵サーバ（オニオン）](http://zkaan2xfbuxia2wpf7ofnkbz6r5zdbbvxbunvp5g2iebopbfc4iqmbad.onion/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D)
- [SHA256 の PGP 署名](alice2bob-SHA256-2.4.sig)
- [SHA256 チェックサム](alice2bob-SHA256-2.4)

誰でも本シリーズを編集して頒布できることを意識してください。
入手したファイルを開いたり内容を信頼する前に、
入手したファイルを検証すべきです。
入手したファイルをアリスとボブが実際に作成したと確認するには、
PGP 鍵とチェックサムを検証して、
チェックサムを入手したファイルに照合してください。

## 検証方法 {#how-to-verify}

本シリーズのファイルを検証するには以下のツールが必要です。

- `sha256sum` のプログラムが入っている Bash ターミナル
- [GnuPG][GnuPG] （プログラム名： `gpg`）

[GnuPG]: https://gnupg.org

### PGP 鍵の指紋 {#confirm-PGP-fingerprint}

PGP 鍵、チェックサム、署名とファイルを入手した後、
まず PGP 鍵の指紋（上の 40 桁の文字列）を上記の指紋に照合します。

~~~
$ gpg --show-key 
~~~

### PGP 鍵のインポート {#import-PGP-key}

PGP 鍵の指紋が正しかったら、インポートします。

~~~
$ gpg --import 
~~~

PGP 鍵のインポートが成功したことを確認します。

~~~
$ gpg -k 1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D
~~~

### 署名検証 {#verify-signature}

SHA256 チェックサムファイルを署名ファイルで検証します。

~~~
$ gpg --verify alice2bob-SHA256-2.4.sig alice2bob-SHA256-2.4
~~~

`Good signature from "alice2bob"`...
（またはそれに類似した日本語）が表示された場合、
チェックサムが破損・改竄されていない可能性が高いということです。

### SHA256 検証 {#verify-checksums}

最後に、入手したファイルを SHA256 チェックサムで検証します。

~~~
$ sha256sum --check --ignore-missing alice2bob-SHA256-2.4
~~~

`OK` の表示が出たら、ファイルが破損・改竄されていない
可能性が高いということです。

入手したファイルのディレクトリ構造が平らの場合、
SHA256 チェックサムファイル内のファィル名から
ディレクトリを削除する必要があります。

~~~
$ cat alice2bob-SHA256-2.4 \
  | sed -e 's/  .*\//  /' \
  | sha256sum --check --ignore-missing
~~~

SHA256 の照合には [GtkHash][GtkHash] を使うことも可能です。

[GtkHash]: https://gtkhash.org
