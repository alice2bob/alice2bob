<img class="top-banner" src="../assets/img/top-banner.png" width="780" height="204" alt="アリスとボブのバナー">

# Tails を用いた自由かつ匿名コンピューティング {#top}

アリスとボブ

バージョン： 2.4

本ガイドの最終更新： 2024-08-16

Tails 6.6

ガイド一覧に戻る
--- [Markdown](../ja-md/alice2bob-ja-preface-2.4.md#list)
\| [HTML](../ja-html/alice2bob-ja-preface-2.4.html#list)

***

1. [Tails とは](#what-is-Tails)
2. [特徴](#features)
   1. [記憶喪失](#amnesia)
   2. [プライバシーと匿名性](#privacy-anonymity)
   3. [セキュリティ](#security)
   4. [持ち運び可能性](#portability)
   5. [Debian ベース](#Debian)
   6. [永続性ストレージ](#persistent-storage)
3. [注意点と限界](#warnings-and-limitations)
   1. [DVD や USB メモリの耐久性と寿命の問題](#media-durability-lifetime)
   2. [性能問題](#performance-issues)
   3. [容量の小さい USB メモリ](#small-USB-sticks)
   4. [ホストデバイスとの互換性の問題](#compatibility-issues)
   5. [Tor 以外のトラフィックの遮断](#block-non-Tor)
   6. [プライバシーや匿名性や秘匿性を保証しない](#no-guarantee)
   7. [永続性ストレージに関する注意](#warning-about-persistence)
   8. [Tails の使用を隠さない](#Tails-doesnt-hide-Tails-use)
   9. [悪質または危殆化したハードウェア](#bad-device)
4. [準備](#prepare)
   1. [ダウンロード](#download)
   2. [検証](#verify)
   3. [インストール](#install)
   4. [ホストデバイスでの起動](#boot)
   5. [ウェルカムスクリーン](#Welcome-Screen)
5. [使い方](#how-to-use)
   1. [ユーザインターフェイス](#user-interface)
   2. [Tails の管理](#Tails-management)
   3. [永続性ストレージ](#persistence)
6. [使用事例](#use-cases)
   1. [ホストデバイスの OS の回避](#ignore-host-device-OS)
   2. [有害の恐れのあるファイルの扱い](#inspecting-malicious-files)

*注： Tails の公式サイト URL が 2023 年 7 月に変わりました。
`https://tails.boum.org/` → `https://tails.net/`*

*注：本ガイドに記載した Tails ドキュメンテーション
（`https://tails.net/doc/`...）への一部の外部リンクは、
Tails 内からもインターネットなしで閲覧できます。
"Applications" → "Tails" → "Tails documentation" の手順で
Tails 内のドキュメンテーションを閲覧します。
ただし、 Tails ウェブサイトと Tails 内のドキュメンテーションは
完全一致しません。*

*注：リリース 5.8 では Tails が大きく変化し、
それに伴って様々な使用上の問題が報告されました。
Tails 5.7 またはそれ以前からの更新を検討している場合、
Tails 5.8 リリース記事内の "Known issues" を参考にしてください。*

→ 外部資料：
Tails 5.8 問題：
`https://tails.net/news/version_5.8/index.en.html#issues`

# Tails とは {#what-is-Tails}

**Tails** （テイルス）とは、
プライバシーと匿名性の保護に特化したデスクトップ OS です。
名称の由来は "The Amnesic Incognito Live System" の頭文字です。

- Amnesic：
  ホストデバイス[^host-device]に何の情報や跡も残さない記憶喪失。
- Incognito：
  プライバシーと匿名性を保護。
- Live：
  持ち運び可能な DVD または USB からすぐに起動。
- System：
  OS[^OS]。

# 特徴 {#features}

上の "TAILS" 理念を実現する特徴を紹介します。

## 記憶喪失 {#amnesia}

Tails が起動すると、基本的には白紙の状態で始まります。
利用者がアクセスしようとしない限り、
Tails はホストデバイスの記憶装置（SSD や HDD）を使用せず、
メモリ（RAM）上のみで動作します。
Tails が終了すると、利用者の情報や作業の跡がホストデバイスに残りません。

白紙状態と記憶に関する例外は
[永続性ストレージ](#persistent-storage)機能です。
Tails の記憶装置（例えば USB メモリ）に永続性ストレージも存在し、
起動の際に利用者が復号化パスワードを入力すると、
Tails から永続性ストレージへのアクセスが可能になります。

## プライバシーと匿名性 {#privacy-anonymity}

Tails はプライバシーと匿名性を保護しようとします。

基本的には白紙の状態で始まり、
OS の唯一の特徴を最小限にします。
そのため、 Tails 利用者同士の区別を困難にしたり、
利用者の作業が OS の唯一の特徴によって
汚染されるリスクを最小限にします。

ほぼ全てのトラフィックを Tor （匿名化ネットワーク）経由にすることで、
ネットワーク上の匿名性を保護しようとします。
その上、 Tor Browser を用意しているため、
簡単に匿名ウェブ閲覧できるようになっています。

→ ガイド：
**Tor 匿名化ネットワーク**
--- [Markdown](../ja-md/alice2bob-ja-Tor-2.4.md)
\| [HTML](../ja-html/alice2bob-ja-Tor-2.4.html)

Tails はネットワークに接続する時に、
ホストデバイスのネットワークデバイスの
MAC アドレス[^MAC-address]をデフォルトでごまかします。
利用者が自身のデバイスを他人のネットワークに接続する場合、
デバイスを特定する唯一の MAC アドレスを
ネットワークから保護するため重要です。

その代わりに、ネットワークが不要な場合、  STFU 原則に則って、
ネットワークを無効化した上で Tails を起動させることができます。

## FLOSS {#FLOSS}

Tails は Debian （Linux）ベースの OS で、 FLOSS です。
私有ソフトウェアの OS より検証可能な Debian をベースに、
Tails は利用者のプライバシーと匿名性の保護に特化しています。

→ ガイド：
**デジタルセキュリティの概念 § FLOSS を使用する**
--- [Markdown](../ja-md/alice2bob-ja-security-concepts-2.4.md#floss)
\| [HTML](../ja-html/alice2bob-ja-security-concepts-2.4.html#floss)

Tails には様々なセキュリティ・プライバシーツールが用意されています。

- **GnuPG**：
  PGP 鍵の生成・管理とデータの暗号化・復号化・署名・検証。
- **KeePassXC**：
  パスワード管理。
- **LUKS**：
  ファイルシステムやディスクの暗号化・復号化。
- **mat2**：
  ファイル内のメタデータの消去。
- **OnionShare**：
  オニオンサービスを用いた Tor ネットワーク経由ファイル共有。
- **Tor** と **Tor Browser**：
  匿名化ネットワークとウェブブラウザ。
- **VeraCrypt**：
  ファイルシステムやディスクの暗号化・復号化。
- ハードウェアキーボードを信用しない場合に使える画面上の仮想キーボード。

Tails のイメージファイルと共に、
それを保護する PGP 署名も入手して検証できます。
そのため、 DVD または USB メモリに書き込んで使用する前に、
イメージファイルが健全で実際に
Tails 開発者がリリースしたか検証できます。
詳しくは[「検証」](#verify)を参照してください。

## 持ち運び可能性 {#portability}

SSD または HDD にインストールする OS と異なり、
Tails はイメージファイルとして入手し、
DVD や USB メモリなど持ち込み可能な記憶装置に書き込む OS です。
DVD または USB メモリに対応できる多くのコンピュータに
起動させることができます。
使い始めやすくて、持ち込みの利便性が高いです。

Tails はホストデバイスの SDD や HDD を使用せず、
ホストデバイスデバイスの OS を無視して起動できるため、
利用者が信用する Tails との互換性があるどのデバイスにでも使えます。
ある作業をするために通常に使う OS でなく Tails を使いたい場合、
他人または公共のデバイスの OS が嫌いため Tails を使いたい場合など、
様々な用途があります。

## Debian ベース {#Debian}

Tails は Debian ベースの OS です。
そのため、 Debian に慣れている人にとって Tails は使いやすいです。

Tails に用意されていないパッケージをインストールしたい場合、
Debian パッケージ（`.deb` 拡張子）を入手し、
Tails にインストールします。
Debian と同様、 APT と dpkg で Debian パッケージを管理します。

Debian によく使われているパッケージが Tails に用意されています。

- **Bash** ターミナルと **GNU コアユーティリティ**（GNU coreutils）：
  コマンド入力やスクリプトを用いたコマンド実行。
- **GIMP**：
  ラスター画像の編集。
- **Git**：
  ソースコードのバージョン管理。
- **Inkscape**：
  ベクター画像の編集。
- **LibreOffice**：
  ドキュメント編集。
- **Thunderbird**：
  電子メールクライアント。
- その他：テキストエディタ、動画再生、ファイルブラウザなど。

## 永続性ストレージ {#persistent-storage}

Tails には LUKS 暗号化永久性ストレージ機能があります。
永久性ストレージには個人ファイル、 PGP 鍵と GnuPG 設定、 SSH 鍵、
ドットファイル[^dotfile]、パッケージ、ネットワーク接続、
ブラウザのブックマークなどを保存でき、
利用者がそれらを有効化したり無効化したりできます。

永久性ストレージが存在する場合、
Tails を起動させる度に復号化パスワードが求められます。
そこで永久性ストレージを開けるか開けないかを選択できます。

→ 外部資料：
永久性ストレージ：
`https://tails.net/doc/persistent_storage/`

# 注意点と限界 {#warnings-and-limitations}

## DVD や USB メモリの耐久性と寿命の問題 {#media-durability-lifetime}

Tails を頻繁にまたは長年にかけて利用したい場合、
記憶媒体（DVD や USB メモリ）の耐久性と寿命を考えなければなりません。
特に、 Tails の永続性ストレージ機能を有効化した場合、
記憶媒体が劣化したら、個人データを失う恐れがあります。

例えば、 USB メモリにインストールした Tails を頻繁に利用したら、
USB メモリの差し込みと取り出しの繰り返しにつれてコネクタが徐々に劣化し、
USB メモリにデータを書き込む度に記憶装置が徐々に劣化します。
それに加えて、 USB メモリは高密度の複雑な電子回路であり、
いつでも故障し得ると思った方がいいでしょう。

その対策として、永続性ストレージ内の個人データや設定などを
定期的に他の記憶媒体にバックアップすることが重要です。
ただし、バックアップする際、 Tails と同様、
パスワードで暗号化したファイルシステム（例： LUKS）に
データをバックアップすることを推奨します。

## 性能問題 {#performance-issues}

SSD または HDD から起動する OS と異なり、
Tails は DVD または USB メモリから起動します。
そのため、プログラムの実行やデータの読み書きに
時間がかかることがあります。

## 容量の小さい USB メモリ {#small-USB-sticks}

Tails の USB メモリからの起動には、
8 GiB 以上の容量を有する USB メモリが必要です。

Tails のイメージはその容量の全てを満たさず、
永続性ストレージを追加できるかもしれません。

## ホストデバイスとの互換性の問題 {#compatibility-issues}

Tails を起動できないデバイスがあります。
まず、 Tails は x86-64[^x86-64] 対応 CPU を必要とし、
スマートフォンや古いコンピュータでは動作しません。
2 GiB 以上の RAM が推奨されます。
また、ホストデバイスの周辺機器を操作できるドライバが
Tails に含まれていない場合、その周辺機器が使えません。
もちろん、 DVD または USB から起動する機能も必要です。

→ 外部資料：
`https://tails.net/doc/about/requirements/`

## Tor 以外のトラフィックの遮断 {#block-non-Tor}

Tails はほぼ全てのトラフィックを Tor ネットワーク経由にし、
Tor 以外のほぼ全てのトラフィックの送受信を遮断します。
そのため、 Tor の問題と限界を引き継ぎます。
例えば、ゲームやビデオ電話など UDP 通信に依存する
好きなアプリケーションが使用できません。

→ ガイド：
**Tor 匿名化ネットワーク § 注意点と限界**
--- [Markdown](../ja-md/alice2bob-ja-Tor-2.4.md#warnings-and-limitations)
\| [HTML](../ja-html/alice2bob-ja-Tor-2.4.html#warnings-and-limitations)

→ 外部資料：
Tor に関する注意点：
`https://tails.net/doc/about/warnings/tor/`

## プライバシーや匿名性や秘匿性を保証しない {#no-guarantee}

Tails はプライバシーや匿名性を保証しません。
記憶喪失、個人を特定する特徴の最小化、
セキュリティ向上などの特徴があるが、
アプリケーションによる個人情報や IP アドレスなどの情報漏洩や
利用者自身の行動による情報漏洩の防止またはその情報の匿名化をしません。
例えば、ある 1 つの Tor Browser セッションから
2 つの異なる目的・活動のアカウントで同じウェブサイトにログインしたり、
メタデータを含む写真をアップロードしたりして、
誤ってアカウント間の繋がりができたり情報漏洩したりしてしまいます。

→ 外部資料：
個人情報の情報漏洩に関する注意点：
`https://tails.net/doc/about/warnings/identity/`

## 永続性ストレージに関する注意 {#warning-about-persistence}

特に、利用者が永続性ストレージを使うと、
永続性ストレージ内の個人ファイルや個人設定によって
利用者の特定や匿名性の低下につながる恐れがあります。
もちろん、マルウェアが永続性ストレージに入っていると、
利用者の情報や Tails の使用が漏洩・操作される恐れがあります。
永続性ストレージの設定と使用を最低限にすることを推奨します。

永続性ストレージのファイルや設定によって
異なる活動（例えば、個人生活とデモ計画）がリンクされてしまわないよう、
各活動をそれぞれ違う Tails 記憶装置で行うことが推奨されています。

→ 外部資料：
Tails に関する注意点：
`https://tails.net/doc/about/warnings/`

## Tails の使用を隠さない {#Tails-doesnt-hide-Tails-use}

Tails は Tails の使用を隠しません。
ローカルネットワークの管理者やインターネットサービスは
利用者が Tails を使っていると推測できるかもしれません。
また、画面が見える身の回りの人たちは利用者が
Tails を使っていると認識するかもしれません。

## 悪質または危殆化したハードウェア {#bad-device}

Tails ではホストデバイスの OS が起動しないため、
Tails は悪質または危殆化したホストデバイスの OS を回避できます。
しかし、悪質または危殆化したホストデバイス（BIOS、ハードウェアなど）
から利用者を保護できません。
攻撃者がキーロガー、悪質な BIOS、
メモリの危殆化、 USB ポートの危殆化などで
ホストデバイスを危殆化させた場合、
Tails はこのような攻撃から利用者を保護できません。

自分のデバイスを使用していない時に、
デバイスを隠したり他人に届かない場所にデバイスを保管したりして、
できる限りデバイスの物理的セキュリティを保護することを推奨します。

→ 外部資料：
疑わしいデバイスを使用時のリスク軽減：
`https://tails.net/doc/about/warnings/computer/`

# 準備 {#prepare}

本セクションでは、以下のデバイスを用いた手順が存在します。

- 現在使用中のデバイス：
  Tails の準備に用いるデバイス（例：パソコン）。
- ホストデバイス：
  Tails を起動させるデバイス（例：パソコン）。
- Tails の記憶装置：
  Tails を保存する記憶装置（例： USB メモリ）。

*注：危殆化したデバイスを用いて
Tails のダウンロード、検証またはインストールを行った場合、
安全でない Tails を準備する恐れがあります。
信用するデバイス（ハードウェア、 BIOS と OS が安全なもの）
を用いて Tails を準備してください。*

## ダウンロード {#download}

Tails ダウンロードページは以下のリンクでアクセスします。

`https://tails.net/install/`

ここから 3 種類の Tails イメージファイルをダウンロードできます。

- USB メモリ用（"For USB sticks (USB image)"）
- DVD 用（"For DVDs (ISO image)"）
- 仮想マシン用（"For virtual machines (ISO image)"）

## 検証 {#verify}

Tails をインストールする前に、
入手した Tails イメージファイルを必ず検証してください。

Tails をダウンロードする際、以下を入手します。

- イメージファイル
- イメージファイルの PGP 署名
- Tails 開発者の PGP 公開鍵

Tails のオンラインドキュメンテーションに
Tails の全ての PGP 公開鍵の情報が記載されています。
そのページに "Signing key" というセクションがあります。

`https://tails.net/doc/about/openpgp_keys/#index2h1`

公開鍵 `<key-file>` （例： `tails-signing.key`）の指紋を照合し、
GnuPG の鍵輪に追加します。

~~~
$ gpg --show-key <key-file>
... （鍵の情報の表示）
$ gpg --import <key-file>
~~~

公開鍵のインポートが成功したことを確認します。

~~~
$ gpg -k tails@boum.org
~~~

イメージファイル `<file>` （例： `tails-amd64-4.25.img`）
を署名 `<signature>` （例： `tails-amd64-4.25.img.sig`）
で検証します。

~~~
$ gpg --verify <signature> <file>
~~~

`Good signature from "Tails developers
(offline long-term identity key)"`... の表示が出たら、
チェックサムが破損・改竄されていない可能性が高いということです。

## インストール {#install}

本セクションでは、コマンドラインを用いた
USB メモリへのインストールを解説します。
Bash ターミナルのコマンドラインを用意した
Linux コンピュータを前提にします。
それと異なる場合、コマンドの詳細（例：デバイスファイル名）
が異なるかもしれません。

*注：本セクションのステップを実行すると
USB メモリ内のデータを全て失います。*

1. 現在使用中のデバイスに存在するデバイスファイルを確認する。
2. USB メモリを差し込む。
3. 再びデバイスファイルを確認する。
4. USB メモリ（デバイス全体）のデバイスファイルを特定する。
5. ルート権限で USB メモリへの書き込みを実行する。

まず、現在使用中のデバイスに存在するデバイスファイルを確認します。
このステップは、誤って USB メモリ以外のデバイスに
Tails のイメージを書き込むことを避けるためです。

~~~
$ ls /dev/sd*
~~~

OS、ファイルシステムの数、周辺機器などによるが、
以下のような出力が表示されるはずです。

~~~
/dev/sda  /dev/sda1  /dev/sda2
~~~

USB メモリを差し込んで、その後に同じコマンドを再実行します。

~~~
$ ls /dev/sd*
/dev/sda  /dev/sda1  /dev/sda2  /dev/sdc  /dev/sdc1
~~~

このように新しい項目が表示されるはずです。
Debian 族の Linux では、
名前が数字で終わる項目（例： `/dev/sdc1`）は
USB メモリ内のファイルシステムで、
1 件以上存在するか存在しないかもしれません。
名前が数字で終わらない項目（例： `/dev/sdc`）は
USB メモリのデバイスの全体で、
書き込み対象項目はこれです。

最後に、ルート権限で書き込みを実行します。
データをコピーするコマンド `dd` では、
`if=` で読み込み先（Tails イメージファイル）
`<file>` （例： `tails-amd64-4.25.img`）を指定し、
`of=` で書き込み先（USB メモリ）
`<USB-memory>` （例： `/dev/sdc`）を指定します。
ここで正しい書き込み先の項目の名前を入力するよう注意してください。

~~~
# dd if=<file> of=<USB-memory> bs=16M; sync
~~~

コマンドが終了したら、 Tails は準備完了です。

## ホストデバイスでの起動 {#boot}

Tails の起動が失敗してホストデバイスの元の OS が起動してしまったり、
ホストデバイスの BIOS・UEFI 設定変更が必要だったりする可能性があるため、
あるホストデバイス上での初めての Tails 起動には
ある程度の注意が必要です。

1. 電源の切れたホストデバイスに USB メモリを差し込む。
2. ホストデバイスのブート（起動）選択キーを把握する。
3. ホストデバイスの電源を入れる。
4. ブート選択キーを押し続けることで、ブート選択メニューを表示させる。
5. USB メモリから Tails を起動させる。

電源の切れたホストデバイスに USB メモリを差し込みます。

ホストデバイスの電源を入れる前に、
ホストデバイスのブート（起動）選択キーを把握します。
<kbd>Esc</kbd> または <kbd>F12</kbd>
のキーをブート選択に用いるデバイスが多いです。
あるいは、ホストデバイスのブートの優先順位を
USB または DVD が最優先となるよう設定しておき、電源を切ります。
ブート選択キーやブートの設定方法は
デバイスのモデルまたは製造者によって異なるため、
わからない場合はデバイスの取扱説明書を参照してください。

電源を入れます。
ブート選択キーを押し続ける必要がある場合、
ブート選択メニューが表示されるまで押し続けます。
ブート選択メニューが表示されたら、 USB メモリからの起動を選びます。

ここまでうまく行くと、次に Tails の起動メニューが表示されるはずです。

→ 外部資料：
Tails の起動（Mac 以外のパソコン）：
`https://tails.net/doc/first_steps/start/pc/`

→ 外部資料：
Tails の起動（Mac）：
`https://tails.net/doc/first_steps/start/mac/`

## ウェルカムスクリーン {#Welcome-Screen}

<img class="display" src="../assets/img/Tails/Welcome-Screen-overview.png" width="636" height="613" alt="ウェルカムスクリーン">

Tails が起動する度に、ウェルカムスクリーン（Welcome Screen）
が表示されます。
Tails の起動が完了する前に、ここでいくつかの設定を変更できます。

→ 外部資料：
ウェルカムスクリーン：
`https://tails.net/doc/first_steps/welcome_screen/`

### ユニバーサルアクセス {#Welcome-Screen-accessibility}

ユニバーサルアクセス機能（コントラスト、ズーム、仮想キーボードなど）
が必要な場合、画面の右上の
<img src="../assets/img/Tails/accessibility-icon.png" width="20" height="20" alt="アイコン">
アイコンをクリックします。

### 言語・地域設定 {#language-region-settings}

<img class="display" src="../assets/img/Tails/Welcome-Screen-locale.png" width="636" height="207" alt="言語・地域設定">

言語・地域設定（Language \& Region）は上に表示されます。

- 言語（Language）：
  <kbd>Alt</kbd>+<kbd>L</kbd>
- キーボード（Keyboard Layout）：
  <kbd>Alt</kbd>+<kbd>K</kbd>
- 日時などの表示を制御する地域設定（Formats）：
  <kbd>Alt</kbd>+<kbd>F</kbd>

### 永続性ストレージ {#Welcome-persistence}

<img class="display" src="../assets/img/Tails/Welcome-Screen-persistence-new.png" width="636" height="183" alt="永続性ストレージの作成オプション">

永続性ストレージがまだ作成されていない場合、
中央に永続性ストレージの作成オプションが表示されます。

<img class="display" src="../assets/img/Tails/Welcome-Screen-persistence-exist.png" width="533" height="108" alt="復号化パスワードの入力欄">

永続性ストレージが既に作成された場合、
中央に復号化パスワードの入力欄が表示されます。
復号化パスワードを入力したら、永続性ストレージがロック解除されます。
<kbd>Alt</kbd>+<kbd>P</kbd> で入力欄に移動します。
永続性ストレージをロック解除後、永続性ストレージなしで
Tails を使用するには Tails を再起動させる必要があります。

### 追加設定 {#additional-settings}

<img class="display" src="../assets/img/Tails/Welcome-Screen-additional.png" width="636" height="177" alt="追加設定">

追加設定（Additional Settings）は下に表示されます。
<kbd>Alt</kbd>+<kbd>A</kbd> を押すか
"+" 記号をクリックすると、追加設定の一覧が表示されます。

#### ルート権限のパスワード {#root-password}

パッケージのインストールなどシステム管理の権限が必要な場合、
ルート（管理者）権限のパスワード（Administration Password）
を指定する必要があります。
ルート権限のパスワードを指定しなければルートアカウントが無効化されます。

攻撃者がルート権限のパスワードを入手すると、
Tails の全ての振る舞いとセキュリティ対策を変えることができるため、
ルート権限の有効化とパスワードの選択・使用に注意が必要です。

<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>A</kbd> を押すと
ルート権限のパスワードの入力欄に移動します。
"Administrator Password" と "Confirm" の欄に
一致するパスワードを入力することで、ルート権限を有効化します。

→ 外部資料：
ルート権限のパスワード：
`https://tails.net/doc/first_steps/welcome_screen/administration_password/`

#### MAC アドレスのランダム化 {#MAC-address}

MAC アドレスのランダム化（MAC Address Anonymization）は
ホストデバイスの MAC アドレスを保護するために
デフォルトで有効化されます。
MAC アドレスのランダム化に伴う問題が特に無ければ、
設定をそのままにすることを推奨します。

公共のコンピュータまたは自宅のネットワークを使用している場合、
ランダム化はおそらく不要でしょう。
また、認定済みの MAC アドレスからの接続だけを
許可するネットワークが存在します。
ランダム化がネットワーク使用上の技術問題または
疑いを生じさせる恐れがある場合、
ランダム化を無効化してください。

ホストデバイスのハードウェア内の制限によって
MAC アドレスのランダム化が失敗する場合があります。
ランダム化に失敗したネットワークデバイスは無効化されます。
ランダム化なしでそのネットワークデバイスを使用したい場合、
無効化してください。

<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>M</kbd> を押すと
MAC アドレスのランダム化の設定に移動します。
"Don't anonymize MAC addresses" を選ぶことで
MAC アドレスのランダム化を無効化します。

→ 外部資料：
MAC アドレスのランダム化：
`https://tails.net/doc/first_steps/welcome_screen/mac_spoofing/`

#### オフラインモード {#offline-mode}

**オフラインモード**（Offline Mode）では、
ホストデバイスのネットワークデバイスを無効化できます。
デバイスからの電波の放射を少なくし（STFU 原則）、
セキュリティを強化させるために、
ネットワークが不要でない限り
オフラインモードを有効化することを推奨します。

オフラインモードを有効化するには、
"Disable all networking" を選びます。

#### Unsafe Browser {#Unsafe-Browser}

**Unsafe Browser** とは、
Tor の使用が不可能または不適切でない時に使用できるブラウザです。
インターネットアクセスの許可を得るために
カプティブポータル[^captive-portal]でログインしたい場合、
ローカルのネットワーク上のプリンタや無線 LAN の管理をしたい場合などに、
Tor ネットワークを使用しない Unsafe Browser が必要です。
このような場合、 Unsafe Browser を有効化してください。

Unsafe Browser を使ってカプティブポータルでのログインに成功して
インターネットアクセスを得た後、 Unsafe Browser を閉じてください。
Tails が Tor 回線を構築しようとし、
Tor 回線の構築ができたら Tor Browser などが使えるようになります。
間違えて Unsafe Browser でウェブ閲覧をしないよう注意してください。

攻撃者が Tails 内の脆弱性を利用することで、
Unsafe Browser を通じて Tails 利用者の IP アドレスを
抽出することがあり得ます。
例えば、攻撃者が Tails 利用者に Thunderbird の脆弱性を利用する
メールで攻撃できるかもしれません。
Unsafe Browser が不要な場合、無効化したままにしておいてください。

Unsafe Browser を有効化するには、
"Enable the Unsafe Browser" を選びます。

### Tails の起動を完了させる {#complete-launching-Tails}

ウェルカムスクリーンが終了して Tails の起動を完了させるには、
<kbd>Alt</kbd>+<kbd>S</kbd> を押すか
ウィンドウの右上側の "Start Tails" ボタンをクリックします。

### Tails を終了させる {#shutdown-from-Welcome-Screen}

ウェルカムスクリーンからすぐに Tails を終了させるには、
ウィンドウの左上側の "Shutdown" ボタンをクリックします。

# 使い方 {#how-to-use}

## ユーザインターフェイス {#user-interface}

Tails の起動が完了したら、すぐに以下のものが見えるはずです。

- メニューバー（上）
  - アプリケーションメニュー（Applications）
  - ディレクトリ一覧（Places）
  - フォーカス中のアプリケーションのメニュー（最初は非表示）
  - 現在の日時
  - クリップボードのアイコン
  - Tor 回線のアイコン
  - ユニバーサルアクセスのアイコン
    <img src="../assets/img/Tails/accessibility-icon.png" width="20" height="20" alt="アイコン">
  - 入力モードのアイコン（日本語、英語など）
  - システムメニュー（音量、電源を切るボタンなど）
- デスクトップ（中央）
- アプリケーションとワークスペースバー（下）
  - 開いているアプリケーション（最初は非表示）
  - ワークスペース切り替えボタン

→ 外部資料：
Tails のデスクトップ：
`https://tails.net/doc/first_steps/desktop/`

### アプリケーションメニュー {#application-menu}

<img class="display" src="../assets/img/Tails/user-interface-applications.png" width="541" height="561" alt="アプリケーションメニュー">

アプリケーションメニューを開くには、
左上の "Applications" をクリックします。

### ディレクトリ一覧 {#places}

<img class="display" src="../assets/img/Tails/user-interface-places.png" width="281" height="506" alt="ディレクトリ一覧">

ディレクトリ一覧（ホーム、永続性ストレージ、
ネットワークなど）を開くには、左上の "Places"
（アプリケーションメニューの右）をクリックします。

### Tor 回線のアイコン {#tor-onion}

メニューバーには Tor 回線の状態が表示されます。
Tor ネットワークに接続されている場合
<img src="../assets/img/Tails/tor-onion-on-icon.png" width="20" height="20" alt="アイコン">
が表示され、接続されていない場合
<img src="../assets/img/Tails/tor-onion-off-icon.png" width="20" height="20" alt="アイコン">
が表示されます。

### ユニバーサルアクセス機能 {#accessibility-functions}

<img class="display" src="../assets/img/Tails/user-interface-accessibility.png" width="240" height="500" alt="ユニバーサルアクセス機能の一覧">

ユニバーサルアクセス機能（コントラスト、ズーム、
仮想キーボードなど）を使用するには、右上の
<img src="../assets/img/Tails/accessibility-icon.png" width="20" height="20" alt="アイコン">
アイコンをクリックします。

### キーボードの入力モード {#keyboard-input-method}

<img class="display" src="../assets/img/Tails/user-interface-keyboard.png" width="263" height="385" alt="入力モードの一覧（メニュー）">
<img class="display" src="../assets/img/Tails/user-interface-super-space.png" width="900" height="175" alt="入力モードの一覧（中央）">

キーボードの入力モードを切り替えるには、
ウィンドウの右上側のキーボードの入力モードのメニュー
（ユニバーサルアクセスとシステムメニューとの間）をクリックします。
その代わり、次の入力モード（使用順）に切り替えるには
<kbd>Super</kbd>[^Super]+<kbd>Space</kbd> を押します。
その後に <kbd>Super</kbd> を押し続けたままにしたら、
入力モードの一覧が画面中央に表示されます。
<kbd>Super</kbd>+<kbd>Space</kbd> を繰り返すことで、
次々と入力モードを選ぶことができます。

ある入力モードには複数の文字入力方式があるかもしれません。
例えば、日本語（Anthy や Mozc）には、直接入力、ひらがな、
カタカナなどがあります。
文字入力方式を変更するには、入力モードのメニューをクリックして、
希望の文字入力方式をクリックします。

### システムメニュー {#system-menu}

<img class="display" src="../assets/img/Tails/user-interface-system-menu.png" width="416" height="470" alt="システムメニュー">

右上のシステムメニューを開くと、以下のものにアクセスできます。

- 電力の状態と設定
- スクリーンショット機能
- 環境設定
  <img src="../assets/img/Tails/system-menu-settings-icon.png" width="20" height="20" alt="アイコン">
- 音量の調整
- 明るさの調整
- ネットワーク接続
- ナイトライトとダークモード
- オフラインモード
  <img src="../assets/img/Tails/system-menu-airplane-icon.png" width="20" height="20" alt="アイコン">
- 画面ロック
  <img src="../assets/img/Tails/system-menu-screen-lock-icon.png" width="20" height="20" alt="アイコン">
- 一時停止
  <img src="../assets/img/Tails/system-menu-suspend-icon.png" width="20" height="20" alt="アイコン">
- 再起動
  <img src="../assets/img/Tails/system-menu-restart-icon.png" width="20" height="20" alt="アイコン">
- 電源を切る
  <img src="../assets/img/Tails/system-menu-shutdown-icon.png" width="20" height="20" alt="アイコン">

### アプリケーションの切り替え {#switching-applications}

<img class="display" src="../assets/img/Tails/user-interface-alt-tab.png" width="610" height="175" alt="アプリケーションの切り替え">

アプリケーションを切り替えるには、
<kbd>Alt</kbd>+<kbd>Tab</kbd> を押します。
その後に <kbd>Alt</kbd> を押し続けたままにしたら、
開いたアプリケーションの一覧（使用順）が画面中央に表示されます。
<kbd>Alt</kbd>+<kbd>Tab</kbd> を繰り返すことで、
次々とアプリケーションを選ぶことができます。

### exposé {#expose}

マウスを左上の角まで勢いよく動かすか
<kbd>Super</kbd>[^Super] を押すと、
以下が表示されます。

- メニューバー（最も上）
- 検索（次に上）
- アプリケーションツールバー（左）
- 開いた全てのアプリケーションのウィンドウ（中央）
- ワークスペース（右）

このモードから戻るには、 <kbd>Esc</kbd> または
<kbd>Super</kbd> を押します。

### 画面ロック {#screen-lock}

特に周りに第三者がいる場所では、
コンピュータから一時的に離れる時に画面をロックすることが賢明です。
既に管理者パスワードが指定された場合、
操作がまったく無い数分間後に画面ロックが自動で行われます。
一方、管理者パスワードが指定されていない場合、
画面ロックが数分間に自動で行われません。

<img class="display" src="../assets/img/Tails/screen-lock.png" width="362" height="206" alt="画面ロック">

画面ロックを有効化するには、
まず右上のシステムメニューから画面ロック
<img src="../assets/img/Tails/system-menu-screen-lock-icon.png" width="20" height="20" alt="アイコン">
を実行して、
解錠パスワードを指定します。
この後、自動画面ロックが有効になり、
操作がまったく無い数分間後に画面がロックされます。
また、右上のシステムメニューからいつでも画面ロックを
実行できるようになります。

## Tails の管理 {#Tails-management}

### 設定 {#Tails-settings}

Tails の設定には以下の手順でアクセスします。

- "Applications" → "System Tools" → "Settings"

デスクトップの背景、通知、デバイス、日時など
様々な設定を調整できます。

Tails の設定は永続しないため、
再起動したら設定変更し直す必要があります。

### 更新 {#update-Tails}

ソフトウェアを最新版に更新することは
重要なセキュリティのための作業です。
以下のように Tails のバージョンを確認します。

- "Applications" → "Tails" → "About Tails"

Tails は自動更新または手動更新の方法で更新できます。

*注： Tails が危殆化した恐れがある場合、
自動更新と手動更新は安全でないかもしれません。
[再インストール](#prepare)で Tails を更新してください。*

→ 外部資料：
Tails の更新：
`https://tails.net/doc/upgrade/`

#### 自動更新 {#automatic-update}

インターネットに接続している場合、
Tails は自動的に更新の有無を確認しようとします。
現在使用中のバージョンより新しいバージョンがあった場合、
Tails は差分更新をダウンロードします。

自動更新が完了した後、 Tails の記憶装置に以下が保存されます。

- Tails のベース（記憶装置に書き込んだ最新のイメージ）
- 差分（ベースバージョンと最新バージョンとの差）

しかし、例えば、起動完了後にインターネット接続がなかった場合、
自動更新が失敗するかもしれません。
Tails 起動完了後に自動更新が失敗している場合、
ターミナルで以下のコマンドを実行してください。

~~~
$ tails-upgrade-frontend-wrapper
~~~

自動更新を何回かすると、差分が徐々に大きくなります。
そのため、差分のダウンロードの時間が徐々に長くなったり
Tails の記憶装置に差分を保存できなくなる可能性があったりします。
自動更新ができなくなった場合、手動更新しかできなくなります。

#### 手動更新 {#manual-update}

手動更新には以下が必要です。

- 更新対象の Tails
- 更新の実行に用いる更新対象以外の Tails （[準備](#prepare)）
- Tails の最新のイメージファイル（[ダウンロード](#download)）

手動更新の手順は以下のとおりです。

1. 更新対象以外の Tails をホストデバイスで起動させる。
2. 起動した Tails に Tails の最新イメージファイルを用意する。
3. Tails Cloner を起動させる。
4. 更新対象の Tails 記憶装置を差し込む。
5. 更新対象の Tails とイメージファイルを参照して、更新を実行する。

Tails Cloner で手動更新を行います。

- "Applications" → "Tails Cloner"

<img class="display" src="../assets/img/Tails/Tails-Cloner.png" width="552" height="543" alt="Tails Cloner">

"Use a downloaded Tails ISO image" を選び、
Tails の最新のイメージファイルを参照します。
"Target USB stick" ドロップダウンに更新対象の Tails を選びます。
更新を実行するには "Install" をクリックします。

また、起動された現在実行中の Tails を他の記憶装置に複製できます。
この場合、 "Clone the current Tails" を選びます。
永久性ストレージも複製したい場合、
"Clone the current Persistent Storage" にチェックを入れます。

### ソフトウェアの追加 {#install-software}

Tails には様々なソフトウェアが事前に用意されているが、
利用者は他のソフトウェアも使いたいかもしれません。

Tails は Debian ベースであるため、
**APT** （Advanced Package Tool）でソフトウェアを管理します。
APT で管理したソフトウェアを**パッケージ**（package）と呼ばれ、
パッケージファイルは `.deb` 識別子を持ちます。

ソフトウェアを追加するには[ルート権限](#root-password)が必要です。

Tails の APT には事前に Debian、 Tails と Tor Project のリポジトリ
（`tor+http` でアクセスするオニオンアドレス）が用意されています。
`/etc/apt/sources.list` と
`/etc/apt/sources.list.d/` を見ることで確認でき、
リポジトリを追加するには `/etc/apt/sources.list.d/` の中に
リポジトリ指定ファイルを追加します。

ソフトウェアをインストールするには、
以下のアプリケーションを使用できます。

- `apt`： APT の一般 CLI コマンド
- Synaptic： APT の GUI アプリケーション
- `dpkg`： `.deb` ファイルを直接扱うコマンド

Synaptic を以下のように開けます。

- "Applications" → "System Tools" → "Synaptic Package Manager"

リポジトリを用いる代わりに、自分でパッケージを入手して、
`dpkg` コマンドで `.deb` ファイルから
パッケージを直接インストールできます。
オフラインモードで Tails を使用する場合にかなり便利な方法だが、
使いたいパッケージとその依存パッケージを
手動に入手する必要があるため困難です。

→ 外部資料：
`https://tails.net/doc/persistent_storage/additional_software/`

→ 外部資料：
`apt(8)` manpage

→ 外部資料：
`dpkg(1)` manpage

### 記憶媒体の自動マウントの無効化 {#disable-automount}

Tails 6.0 以降では USB メモリや外部ハードディスクを差し込んだら
すぐに自動的にマウントされます。
記憶媒体に暗号化ファイルシステム（例： LUKS）がある場合、
すぐにパスワード要求されます。

ただし、画面ロック中、差し込まれた USB デバイスが無視されます。
画面ロックを解除した後、 USB デバイスが差し込まれたという通知が
表示されます。

自動マウントを無効化するには以下のコマンドを実行します。

~~~
$ gsettings set org.gnome.desktop.media-handling automount false
$ gsettings set org.gnome.desktop.media-handling automount-open false
~~~

## 永続性ストレージ {#persistence}

### 作成 {#create-persistence}

永続性ストレージの作成と設定は以下の手順でアクセスします。

- "Applications" → "Tails" → "Persistent Storage"

<img class="display" src="../assets/img/Tails/persistence-create.png" width="796" height="600" alt="永続性ストレージの作成">

永続性ストレージがまだ作成されていない場合、
パスワードの指定が要求されます。
良好なパスワードを決めて、パスワード入力欄に入れます。
その後、作成ボタンをクリックします。

永続性ストレージの作成には多少時間がかかるかもしれません。
作成が完了した後、設定が表示されます。

→ 外部資料：
永久性ストレージの作成：
`https://tails.net/doc/persistent_storage/create/`

### 設定 {#config-persistence}

永続性ストレージの作成と設定は以下の手順でアクセスします。

- "Applications" → "Tails" → "Persistent Storage"

<img class="display" src="../assets/img/Tails/persistence-config-0.png" width="796" height="693" alt="永続性ストレージの設定（前半）">
<img class="display" src="../assets/img/Tails/persistence-config-1.png" width="796" height="693" alt="永続性ストレージの設定（後半）">

以下の項目を永続性ストレージに保存できます。

- 個人ファイル（Persistent Folder）
- ウェルカムスクリーン（Welcome Screen）
- プリンタ（Printers）
- ネットワーク接続（Network Connections）
- Tor ブリッジ（Tor Bridge）
- ブラウザのブックマーク（Tor Browser Bookmarks）
- Electrum ビットコインウォレット（Electrum Bitcoin Wallet）
- 電子メールクライアントデータ（Thunderbird Email Client）
- GnuPG データ（GnuPG）
- Pidgin データ（Pidgin Instant Messenger）
- SSH クライアントデータ（SSH Client）
- インストールしたパッケージ（Additional Software）
- ドットファイル（Dotfiles）

→ 外部資料：
永久性ストレージの設定：
`https://tails.net/doc/persistent_storage/configure/`

### 他の記憶装置へのバックアップ {#backup-persistence}

*注：特にコマンドラインが苦手な人には、本節で説明する方法より
[Tails Cloner](#manual-update) を用いた方法を推奨します。*

永続性ストレージ内の情報をただ一つの記憶装置だけに保存したら、
紛失、盗難、破損などによって情報を失うリスクが高いです。
そのため、他の記憶装置にも Tails と永続性ストレージを準備し、
定期的にバックアップを行うことを推奨します。

1. 外部のバックアップ用 Tails
   を他の記憶装置に[準備](#prepare)する。（初回だけ行う）
2. 現在使用中の Tails の記憶装置から Tails を起動させる。
3. ウェルカムスクリーンで、現在使用中の Tails の永続性ストレージを
   [ロック解除](#Welcome-persistence)し、
   [ルート権限を有効化](#root-password)する。
4. 外部の Tails の永続性ストレージをロック解除する。
5. バックアップを実行する。

バックアップ用 Tails を準備する際、
現在使用中の Tails 記憶装置の大きさと
同等またはより大きい記憶装置を使用して、
イメージファイルを検証した上で
記憶装置にインストールしてください。

バックアップを行う前に、
外部の Tails の永続性ストレージをロック解除します。

- "Applications" → "Accessories" → "Files"
  → 左側で表示される永続性ストレージをクリックする

コマンドラインを用いてバックアップを実行するには、
ルート権限が必要です。

- "Applications" → "System Tools" → "Root Terminal"

ルート権限で以下のコマンドを実行します。

~~~
# rsync -PaSHAXv --del /live/Persistent/TailsData_unlocked/ /media/amnesia/TailsData
~~~

- `/live/Persistent/TailsData_unlocked/`：現在使用中の Tails
- `/media/amnesia/TailsData`：外部の Tails

*注：以上の `rsync` コマンドを実行する前に、
各永続性ストレージの末尾の `/` の有無が
以上と一致することを確認してください。*

コマンドラインを用いる代わりに、
Tails の内蔵バックアップ機能を用いてバックアップを実行できます。

- "Applications" → "Tails" → "Back Up Persistent Storage"

→ 外部資料：
永久性ストレージのバックアップ：
`https://tails.net/doc/persistent_storage/backup/`

### 故障した Tails からの救助 {#rescue-persistence}

Tails が起動しなくなった場合、
記憶装置が劣化しつつあって故障する可能性があるため、
永続性ストレージ内の情報をすぐに救助することを推奨します。

1. 新しい Tails を他の記憶装置に[準備](#prepare)する。
2. 新しい Tails の記憶装置から Tails を起動させる。
3. ウェルカムスクリーンで、新しい Tails の永続性ストレージを
   [ロック解除](#Welcome-persistence)し、
   [ルート権限を有効化](#root-password)する。
4. 劣化している Tails の永続性ストレージをロック解除する。
5. 救助を実行する。

新しい Tails を準備する際、
劣化している Tails 記憶装置の大きさと
同等またはより大きい記憶装置を使用して、
イメージファイルを検証した上で記憶装置にインストールしてください。

救助を行う前に、劣化している Tails の
永続性ストレージをロック解除します。

- "Applications" → "Accessories" → "Files"
  → 左側で表示される永続性ストレージをクリックする。

Files に劣化している Tails の永続性ストレージが表示されない場合、
永続性ストレージが既に故障していて救助できない可能性があります。

コマンドラインを用いて劣化している Tails
の永続性ストレージを救助するには、ルート権限が必要です。

- "Applications" → "System Tools" → "Root Terminal"

ルート権限で以下のコマンドを実行します。

~~~
# rsync -PaSHAXv --del /media/amnesia/TailsData/ /live/Persistent/TailsData_unlocked
~~~

- `/media/amnesia/TailsData/`：劣化している Tails
- `/live/Persistent/TailsData_unlocked`：復元先の Tails

*注：以上の `rsync` コマンドを実行する前に、
各永続性ストレージの末尾の `/` の有無が
以上と一致することを確認してください。*

→ 外部資料：
永久性ストレージの救助：
`https://tails.net/doc/persistent_storage/recover/`

### 削除 {#delete-persistence}

*注：永続性ストレージを以下の方法で削除しても、
高い能力を有する攻撃者がデータ復元の手法で
データを復元できるかもしれません。
安全に情報を削除するには、
記憶媒体内の全てのデータを完全消去する必要があります。*

永続性ストレージの削除は以下の手順でアクセスします。

- "Applications" → "Tails" → "Persistent Storage"

ウィンドウが表示されたら、
"Delete" をクリックすることで永続性ストレージを削除します。

永続性ストレージの削除は、ロック解除された状態で削除できません。
永続性ストレージが既にロック解除されている場合、
Tails を再起動してから永続性ストレージを削除してください。

→ 外部資料：
永久性ストレージの削除：
`https://tails.net/doc/persistent_storage/delete/`

# 使用事例 {#use-cases}

## ホストデバイスの OS の回避 {#ignore-host-device-OS}

Tails をホストデバイスで起動させると、
ホストデバイスの OS に触れることはありません。
そのため、ホストデバイスの（信用しないまたは使いたくない） OS を回避して
Tails をホストデバイス上で使えます。

公共のパソコンまたは他人の所有物であるホストデバイスは、
信用できないものかもしれません。
その場合、以下の点を念頭に置いてください。

- 信用するデバイス上のみで Tails をインストールする。
- 他の OS を実行中のデバイスに Tails の記憶装置を差し込まない。
- Tails の記憶装置で他の OS からまた他の OS へのファイル転送をしない。
- ホストデバイスが怪しいと思ったら、
  記憶装置を差し込んだり永続性ストレージをロック解除したりしない。
- キーボードが危殆化した恐れがある場合、
  Tails の仮想キーボードまたはパスワード管理ソフトウェア
  （例： KeepassXC）を使うことでキーボードの使用をできる限り避ける。

Tails が危殆化した恐れがある場合、
記憶装置を完全消去して Tails をインストールし直してください。

→ 外部資料：
疑わしいデバイスを使用時のリスク軽減：
`https://tails.net/doc/about/warnings/computer/`

## 有害の恐れのあるファイルの扱い {#inspecting-malicious-files}

Tails で有害の恐れのあるファイルをより安全に扱うことができます。

1. Tails を起動させる。
2. 永続性ストレージをロック解除せず、
   [オフラインモード](#offline-mode)を有効化した上で、
   起動を完了させる。
3. 他の記憶装置から Tails に有害の恐れのあるファイルをコピーする。
4. その他の記憶装置を取り出す。
5. 有害の恐れのあるファイルを扱う。

日常的に使用する Tails ではなく、
危殆化しても構わない Tails を起動させることを推奨します。
理想的には、使い捨ての Tails を準備して、それを起動させます。

オフラインモード（インターネットに接続していない状態）で
ファイルを扱うため、ファイルが実際に有害であっても
攻撃者などへの情報抜き出しなどの可能性はないでしょう。

ただし、有害の恐れのあるファイルを
何かしらのアプリケーションで開いた時点に、
ファイルがそのアプリケーションの何かしらの脆弱性を用いて
攻撃を実行したかもしれないため、
その時点から現在使用中の Tails が
危殆化していることを仮定した方がいいです。
したがって、ファイルの扱いを完了したら、
すぐに Tails を終了させることを推奨します。

有害の恐れのあるファイルを扱っても
記憶装置内の Tails イメージと永続性ストレージが
影響を受けるリスクが低いだろうが、その可能性を否定できません。
有害の恐れのあるファイルを扱った後、
記憶装置を完全消去して Tails をインストールし直すと最も安全です。



[^captive-portal]: 多くの公共ネットワークは、
あるデバイスにインターネットアクセスを与える前に、
デバイス利用者に個人情報を提供してもらったり
利用規約に同意してもらったりします。
ネットワーク管理者は「カプティブポータル」というウェブページを
ブラウザに送り付けることで
デバイス利用者のログイン・登録・同意を求めることが多いです。

[^dotfile]: ドットファイル：
`.` でファイル名が始まる隠しファイル、
たいていは設定ファイルやプログラムが暗に作成したファイル。
Linux や macOS では通常のファイル表示からデフォルトで隠されます。

[^host-device]: ホストデバイス：
Tails を起動させるデバイス。

[^MAC-address]: MAC アドレス：
ネットワーク機器を唯一かつ永続的に特定するネットワークアドレス。
"MAC" は "Media Access Control" の頭文字。

[^OS]: OS：
"operating system" （オペレーティングシステム）の頭字語。

[^Super]: <kbd>Super</kbd> キーは、
Windows キーボードの「スタート」キーに当てはまるキーです。

[^x86-64]: Intel または AMD の 64 ビット CPU アーキテクチャ。
やや最近のコンピュータが対応するはずだが、
32 ビット CPU や PowerPC を搭載したコンピュータが非対応。


***

# アリスとボブ {#back}

ガイド一覧に戻る
--- [Markdown](../ja-md/alice2bob-ja-preface-2.4.md#list)
\| [HTML](../ja-html/alice2bob-ja-preface-2.4.html#list)

## 連絡先 {#back-contact}

### PGP 鍵 {#back-contact-PGP}

`1D3E 313C 4DB2 B3E0 8271 FC48 89BB 4CBB 9DBE 7F6D`

- ウェブサイト上：
  `https://git.disroot.org/alice2bob/alice2bob/src/branch/master/alice2bob.asc`
- 鍵サーバ（クリアネット）：
  `https://keys.openpgp.org/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D`
- 鍵サーバ（オニオン）：
  `http://zkaan2xfbuxia2wpf7ofnkbz6r5zdbbvxbunvp5g2iebopbfc4iqmbad.onion/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D`

### ウェブサイト {#back-contact-website}

<img class="qr" src="../assets/qr/contact-website.png" width="222" height="222" alt="ウェブサイト QR コード">

`https://git.disroot.org/alice2bob/alice2bob`

## 検証 {#back-verify}

`https://git.disroot.org/alice2bob/alice2bob/src/branch/master/verify.md`

## 免責事項 {#back-disclaimer}

アリスとボブはセキュリティ専門家でも法律家でも
金融アドバイザーでも医師でもありません。
本シリーズの目的はあくまでも情報提供で、何の助言でもありません。
本シリーズは全くの無保証で提供されます。
本シリーズによってどんな不利、損失または損害が生じても、
アリスとボブは一切の責任を負いません。
ご自身の状況を考慮し、自己責任で本シリーズを使ってください。
