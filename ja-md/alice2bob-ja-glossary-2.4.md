<img class="top-banner" src="../assets/img/top-banner.png" width="780" height="204" alt="アリスとボブのバナー">

# 用語集 {#top}

アリスとボブ

バージョン： 2.4

本ガイドの最終更新： 2022-02-28

ガイド一覧に戻る
--- [Markdown](../ja-md/alice2bob-ja-preface-2.4.md#list)
\| [HTML](../ja-html/alice2bob-ja-preface-2.4.html#list)

***

2FA

:   二要素認証。
    "two-factor authentication" の頭字語。 \
    → 多要素認証

anonymity set

:   → 匿名性集合

CAPTCHA

:   応答者がコンピュータでなく人間であることを確認するための
    チャレンジレスポンス認証。

    変形した英数字の認識、写真に写っている物体の認識、音声認識など
    コンピュータにとって困難な作業が認証の主な形式。

captive portal

:   → カプティブポータル

clearnet

:   → クリアネット

dictionary attack

:   → 辞書攻撃

dotfile

:   → ドットファイル

FLOSS

:   "free/libre and open-source software" の頭字語。 \
    → 自由オープンソースソフトウェア

IP address

:   → IP アドレス

IP アドレス

:   ネットワーク上のデバイスを特定するインターネット上のアドレス。

    インターネット上（IP パケット）で発信され、
    デバイスの地域（国、州、市など）の絞りを可能にする。
    "IP" は "Internet Protocol" の頭文字。

KISS

:   "keep it simple stupid" （単純にしておけの意）の頭字語。

MAC address

:   → MAC アドレス

MAC アドレス

:   ネットワーク機器を唯一かつ永続的に特定するネットワークアドレス。

    ローカルネットワーク上の接続とデータ転送に用いられる。
    "MAC" は "Media Access Control" の頭文字。

MFA

:   多要素認証。
    "multi-factor authentication" の頭字語。 \
    → 多要素認証

operating system

:   → OS

OS

:   "operating system" （オペレーティングシステム）の頭字語。

    アプリケーションとハードウェアとの仲介を、
    アプリケーションを動作させるソフトウェア。
    例：
    Android、
    GrapheneOS、
    iOS、
    Linux、
    macOS、
    Qubes、
    Tails、
    Windows。

rainbow table

:   → レインボーテーブル

side-channel attack

:   → サイドチャネル攻撃

STFU

:   "shut the fuck up" （黙っとけの意）の頭字語。

オペレーティングシステム

:   → OS

カプティブポータル

:   多くの公共ネットワークにおいて、
    あるデバイスにインターネットアクセスを与える前に、
    デバイス利用者のログイン・登録・同意を求めるために
    ブラウザに送り付けられるページ。

    英語で "captive portal"。

キャプチャ

:   → CAPTCHA

クリアネット

:   暗号化が任意な、公がアクセスできる通常のインターネット。
    また、ダークネットまたは匿名化ネットワークの範囲内から出ること。

    英語で "clearnet"。

サイドチャネル攻撃

:   システムの入出力を直接的に操作・観察せず、
    システムの物理的な非計画入出力（サイドチャネル）を操作・観察して、
    漏洩している情報を取得したり故障を注入したりする攻撃。

    コンピュータへの電力供給観測、ソフトウェア実行時のタイミング解析、
    CPU クロック操作などの物理的手段でサイドチャネルを攻撃すること。
    英語で "side-channel attack"。

辞書攻撃

:   人が発送する弱いパスワードによく使われるパターンを登録して、
    攻撃に利用すること。

    英語で "dictionary attack"。

自由オープンソースソフトウェア

:   ソースコードが開示され、利用・コンパイル・頒布・変更ともが
    自由なソフトウェア。

    自由ソフトウェアとオープンソースソフトウェアをまとめる総称。
    英語で "free/libre and open-source software" (FLOSS)。

センシティブ情報

:   慎重に扱うべき、または、公にすべきではない情報。

    「センシティブ」（sensitive）は「敏感」「慎重に扱うべき」などの意。
    秘密な情報だと限らない。
    また、原則として全ての情報をセンシティブ情報として扱うべき。

    センシティブであり得る情報の例：
    個人情報（生年月日、氏名、人種、性別、住所、連絡先、
    身分証明書、社会保障番号など）、
    健康情報、
    身体的生体情報（網膜、虹彩、顔、声、指紋、身体寸法など）、
    行動的生体情報（歩調、感情、執筆の特徴、文体など）、
    日常的な物事（自宅の詳細、通勤通学の経路、車の車種とプレートなど）、
    財産と金銭情報（不動産、高級品、口座残高、支払い履歴など）、
    思想（信条、宗教、政治観など）、
    ソーシャルグラフ（同居者、家族、隣人、恋人、友人、仕事の仲間、
    情報源など）、
    行為情報（人の行為の詳細、発言や作品の出所明示など）、
    活動情報（計画、履歴、関係者、打ち合わせ会場、プロセスなど）、
    能力とスキル、
    セキュリティ対策の詳細、
    秘密情報（顧客情報、知的財産、国家機密など）。
    デジタル界における例：
    デバイスの機種、
    MAC アドレスと IP アドレス、
    IMEI と IMSI、
    個人ファイル、
    インストールされたソフトウェア、
    環境設定、
    クッキー、
    秘密鍵やシードやパスワード、
    行動的生体情報（マウスの動き、スクロール速度、
    打鍵（キーストローク）リズムなど）。

多要素認証

:   アクセス権限を与える前にユーザから
    2 つ以上の要素を必要とする認証方式。

    英語で "multi-factor authentication"。
    二要素認証（two-factor authentication）はよくあるが、
    三要素以上は稀にある。

匿名性集合

:   ある行為をしたことがあり得る匿名候補者の集合。

    匿名性集合に含まれる人数が多いほど匿名性が強い。
    その行為についての事実が知られることが多くなるにつれて、
    候補者が絞られ、匿名性が低下。
    英語で "anonymity set"。

ドットファイル

:   `.` でファイル名が始まる隠しファイル、
    たいていは設定ファイルやプログラムが暗に作成したファイル。

    Linux や macOS では通常のファイル表示からデフォルトで隠される。
    英語で "dotfile"。

レインボーテーブル

:   ハッシュ値から文字列（例：パスワード）を解読するために、
    前もって作成された文字列と対応するハッシュ値の計算結果の表。

    英語で "rainbow table"。


***

# アリスとボブ {#back}

ガイド一覧に戻る
--- [Markdown](../ja-md/alice2bob-ja-preface-2.4.md#list)
\| [HTML](../ja-html/alice2bob-ja-preface-2.4.html#list)

## 連絡先 {#back-contact}

### PGP 鍵 {#back-contact-PGP}

`1D3E 313C 4DB2 B3E0 8271 FC48 89BB 4CBB 9DBE 7F6D`

- ウェブサイト上：
  `https://git.disroot.org/alice2bob/alice2bob/src/branch/master/alice2bob.asc`
- 鍵サーバ（クリアネット）：
  `https://keys.openpgp.org/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D`
- 鍵サーバ（オニオン）：
  `http://zkaan2xfbuxia2wpf7ofnkbz6r5zdbbvxbunvp5g2iebopbfc4iqmbad.onion/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D`

### ウェブサイト {#back-contact-website}

<img class="qr" src="../assets/qr/contact-website.png" width="222" height="222" alt="ウェブサイト QR コード">

`https://git.disroot.org/alice2bob/alice2bob`

## 検証 {#back-verify}

`https://git.disroot.org/alice2bob/alice2bob/src/branch/master/verify.md`

## 免責事項 {#back-disclaimer}

アリスとボブはセキュリティ専門家でも法律家でも
金融アドバイザーでも医師でもありません。
本シリーズの目的はあくまでも情報提供で、何の助言でもありません。
本シリーズは全くの無保証で提供されます。
本シリーズによってどんな不利、損失または損害が生じても、
アリスとボブは一切の責任を負いません。
ご自身の状況を考慮し、自己責任で本シリーズを使ってください。
