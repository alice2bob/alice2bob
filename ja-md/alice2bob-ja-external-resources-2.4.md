<img class="top-banner" src="../assets/img/top-banner.png" width="780" height="204" alt="アリスとボブのバナー">

# 外部資料 {#top}

アリスとボブ

バージョン： 2.4

本ガイドの最終更新： 2024-08-20

ガイド一覧に戻る
--- [Markdown](../ja-md/alice2bob-ja-preface-2.4.md#list)
\| [HTML](../ja-html/alice2bob-ja-preface-2.4.html#list)

***

1. [著作物](#literature)
   1. [English](#English)
   2. [日本語](#Japanese)
2. [ソフトウェアとサービス](#software-and-services)
   1. [ソフトウェア・サービス一覧](#listings)
   2. [ソフトウェア](#software)
   3. [サービス](#services)
3. [市民社会組織](#civil-society-orgs)

# 著作物 {#literature}

## English {#English}

**Jameson Lopp, "Attack Vectors in Real Life: Being your own bank," 2020-09-03** \
`https://invidious.snopyta.org/watch?v=eWbBnqRcIo0` \
*内容：暗号通貨に関する損失・攻撃ベクトル*

**Buggedplanet.info** \
`https://buggedplanet.info` \
*内容： COMINT と SIGINT などに関連する監視技術、
監視対象国・地域など（未検証情報も含む）*

**Bert-Jaap Koops, "Crypto Law Survey"** \
`https://web.archive.org/web/http://www.cryptolaw.org/`

**Osamu Aoki, "Debian Reference"** \
`https://www.debian.org/doc/user-manuals#quick-reference` \
*注：[和文](#Japanese)もあります。*

**Glenorca Borradaile, "Defending Dissent," Oregon State University** \
`https://open.oregonstate.education/defenddissent/` \
参照するページ： `https://blog.torproject.org/book-defend-dissent-with-tor/` \
*注：[和訳](#Japanese)があります。*

**Jacob Appelbaum, "Digital Anti-Repression Workshop," 2012-04-26** \
`https://archive.org/details/JacobAppelbaum-DigitalAnti-repressionWorkshopapril262012.avi`

**Digital Defenders Partnership, "Digital First Aid Kit," 2015** \
`https://www.digitalfirstaid.org/`

**Access Now, "A First Look at Digital Security"** \
`https://www.accessnow.org/first-look-at-digital-security/`

**The Hated One** \
`https://invidious.snopyta.org/thehatedone`

**Tactical Technology Collective, "Holistic Security"** \
`https://holistic-security.tacticaltech.org/`

**ARTICLE 19, "How the Internet Really Works," No Starch Press, 2020-12, ISBN 978-1-7185-0029-7** \
`https://catnip.article19.org/`

**Jens Müller et al., "'Johnny, you are fired!' – Spoofing OpenPGP and S/MIME Signatures in Emails," 2019-08, Proc. 28th USENIX Security Symposium, ISBN 978-1-939133-06-9** \
`https://www.usenix.org/conference/usenixsecurity19/presentation/muller`

**Marc Meillassoux, Mihaela Gladovic, "Nothing to Hide," 2017** \
`https://videocommons.org/w/6b43a28c-86ee-4681-8ab9-787ac140afc0`

**Madaidan's Insecurities** \
`https://madaidans-insecurities.github.io/`

**grugq, "Operational PGP"** \
`https://gist.github.com/grugq/03167bed45e774551155` \
*注：聞き手が不明で、実用性が限られているが、
PGP やメールについてのアドバイスが含まれています。*

**Jonathan "Smuggler" Logan, "Operational Security," 2016-04** \
`https://opaque.link/post/opsecguide/`

**Opt Out Podcast** \
`https://optoutpod.com/` \
`http://optoutkoplzfgs7wl3gkg5nmtrrs7ki6ljcguf7c4w7rdsrtozlghxad.onion/`

**Electronic Frontier Foundation, "Privacy Breakdown of Mobile Phones," Surveillance Self-Defense** \
`https://ssd.eff.org/en/playlist/privacy-breakdown-mobile-phones`

**Håkan Geijer, "RM: OpSec"** \
`https://opsec.riotmedicine.net/`

**Javier Fernández-Sanguino Peña, "Securing Debian Manual"** \
`https://www.debian.org/doc/user-manuals.ja.html#securing` \
*注：[和訳](#Japanese)があります。*

**Tactical Technology Collective, Frontline Defenders, "Security in a Box"** \
`https://securityinabox.org/` \
`http://lxjacvxrozjlxd7pqced7dyefnbityrwqjosuuaqponlg3v7esifrzad.onion/`

**Tor Project, "Selected Papers in Anonymity"** \
`https://www.freehaven.net/anonbib/` \
`http://7fa6xlti5joarlmkuhjaifa47ukgcwz6tfndgax45ocyn4rixm632jid.onion/anonbib/`
*内容：匿名性に関連する文献*

**Northshore Counter Info, "Signal Fails," It's Going Down, 2019-06-03** \
`https://itsgoingdown.org/signal-fails`

**Surveillance Report** \
`https://surveillancereport.tech`

**Electronic Frontier Foundation, "Surveillance Self-Defense"** \
`https://ssd.eff.org/`

**Techlore** \
`https://invidious.snopyta.org/channel/UCs6KfncB4OV6Vug4o_bzijg` \
*内容：大衆向けプライバシーとデジタルセキュリティガイド*

**Totem Project** \
`https://totem-project.org/`

**Crimethinc, "What Is Security Culture?," 2004-11-01** \
`https://crimethinc.com/2004/11/01/what-is-security-culture`

**Phil Zimmermann, "Why I Wrote PGP," 1991** \
`https://www.philzimmermann.com/EN/essays/WhyIWrotePGP.html`

## 日本語 {#Japanese}

**Udonya （ブログ）** \
`http://gpvdip7rd7bdy5gf7scl3rzgzgzckqw4sqxbmy6g3zijfwu4lz3ypbyd.onion/Personal/writings.html` \
*内容：ソフトウェア、プライバシーなど*

**斉藤 英樹『GNU Privacy Guard 講座』** \
`https://gnupg.hclipper.com/` \
*注：一部の内容がかなり古くて、最近更新されていません。*

**kurenaif （GitHub と Youtube コンテンツ）** \
GitHub： `https://github.com/kurenaif/` \
Youtube： `https://invidious.snopyta.org/channel/UCM--uemqoP45daIZG2-VpOA` \
*内容：暗号、脆弱性、攻撃など*

**チャノロジー・ジャパン『サイバー自己防衛 2021』** \
`https://www.anonymous-japan.org/download/` \
*内容： 10friends、 Bisq、 Session*

**Fernández-Sanguino Peña, Javier 『Debian セキュリティマニュアル』** \
`https://www.debian.org/doc/user-manuals.ja.html#securing` \
*注：[英文](#English)もあります。*

**青木 修『Debian リファレンス』** \
`https://www.debian.org/doc/user-manuals.ja.html#quick-reference` \
*注：[英文](#English)もあります。*

**アムネスティ日本『日常生活に入り込むデジタル監視〜あなたのケータイも見られている！〜（ゲスト：小笠原みどりさん）』** \
`https://invidious.snopyta.org/watch?v=QqfSBcr5HM0`

**小倉 利丸『反監視情報』** \
`https://www.alt-movements.org/no_more_capitalism/hankanshi-info/` \
*内容： COVID-19 監視、検閲、暗号化、 Linux など*

**Borradaile, Glenorca 『反対派を防衛する』** \
直接ダウンロード： `https://www.jca.apc.org/jca-net/sites/default/files/2021-11/反対派を防衛する(統合版).pdf` \
参照するページ： `https://www.jca.apc.org/jca-net/ja/node/147` \
*注： ["Defend Dissent"](#English) の和訳。*

# ソフトウェアとサービス {#software-and-services}

## ソフトウェア・サービス一覧 {#listings}

**alternative-front-ends （代替フロントエンド一覧）** \
`https://github.com/mendel5/alternative-front-ends`

**delightful** \
`https://delightful.club/`

**Privacy Tools** \
`https://privacytools.io/` \
`http://privacy2zbidut4m4jyj3ksdqidzkw3uoip2vhvhbvwxbqux5xy5obyd.onion/`

**switching.software** \
`https://swiso.org/`

## ソフトウェア {#software}

**Bisq （非中央化の暗号通貨取引所）** \
`https://bisq.network/`

**Dangerzone （有害かもしれないドキュメントを安全な PDF に変換）** \
`https://dangerzone.rocks/`

**Debian （汎用 Linux OS）** \
`https://www.debian.org/` \
`http://5ekxbftvqg26oir5wle3p27ax3wksbxcecnm6oemju7bjra2pn26s3qd.onion/`

**F-Droid （Android のソフトウェア管理）** \
`https://f-droid.org/`

**Freenet （匿名化ネットワーク）** \
`https://freenetproject.org/`

**GNUnet （匿名化ネットワーク）** \
`https://gnunet.org/`

**GnuPG （暗号コマンドラインツール）** \
`https://gnupg.org/`

**I2P （匿名化ネットワーク）** \
`https://geti2p.net/`

**KeePassDX （パスワード管理）** \
`https://www.keepassdx.com/`

**KeePassXC （パスワード管理）** \
`https://keepassxc.org/`

**Lokinet （匿名化ネットワーク）** \
`https://lokinet.org/`

**Nym （匿名化ネットワーク）** \
`https://nymtech.net/`

**OnionShare （オニオンサービスを用いたファイル共有）** \
`https://onionshare.org/` \
`http://lldan5gahapx5k7iafb3s4ikijc4ni7gx5iywdflkba5y2ezyg6sjgyd.onion/`

**OpenPGP （暗号プロトコル）** \
ウェブサイト： `https://openpgp.org/` \
RFC 4880： `https://datatracker.ietf.org/doc/html/rfc4880`

**Qubes OS （分離でのセキュリティに特化した OS）** \
`https://qubes-os.org/` \
`http://qubesosfasa4zl44o4tws22di6kepyzfeqv3tg4e3ztknltfxqrymdad.onion/`

**Session （メッセンジャー）** \
`https://getsession.org/`

**Tails （持ち運び可能な OS）** \
`https://tails.net/`

**Tor（匿名化ネットワーク）** \
`https://www.torproject.org/` \
`http://2gzyxa5ihm7nsggfxnu52rck2vv4rvmdlkiu3zzui5du4xyclen53wid.onion/`

## サービス {#services}

**Access Now Digital Security Helpline （デジタルセキュリティ緊急連絡）** \
`https://www.accessnow.org/help/`

**AmIUnique （ブラウザ指紋テスト）** \
`https://amiunique.org/`

**AnonAddy （メール転送）** \
`https://anonaddy.com/`

**anonbox （使い捨てメールアドレス）** \
`https://anonbox.net/`

**Autistici/Inventati （様々なサービス）** \
`https://www.autistici.org/`

**Brave Search （検索エンジン）** \
`https://search.brave.com/` \
`https://search.brave4u7jddbv7cyviptqjc7jusxh72uik7zt6adtckl5f4nwy2v72qd.onion/`

**BrowserLeaks （ブラウザ指紋テスト）** \
`https://browserleaks.com/`

**CheckShortURL （短縮 URL の拡張）** \
`https://checkshorturl.com/`

**Codeberg （Forgejo インスタンス）** \
`https://codeberg.org/`

**Dégooglisons Internet （様々なサービス）** \
`https://degooglisons-internet.org/`

**Disroot （様々なサービス）** \
`https://disroot.org/`

**DuckDuckGo （検索エンジン）** \
`https://duckduckgo.com/` \
`https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion/`

**Front Line Defenders Emergency Contact （デジタルセキュリティ緊急連絡）** \
`https://www.frontlinedefenders.org/emergency-contact` \
`http://3g2wfrenve2xcxiotthk4fcsnymzwfbttqbiwveoaox7wxkdh7voouqd.onion/emergency-contact`

**Invidious （Youtube の代替フロントエンド）** \
ウェブサイト： `https://invidious.io/` \
インスタンス一覧： `https://github.com/iv-org/documentation/blob/master/Invidious-Instances.md`

**Just Delete Me （アカウント削除手順一覧）** \
`https://justdeleteme.xyz/`

**keys.openpgp.org （OpenPGP 公開鍵サーバ）** \
`https://keys.openpgp.org/` \
`http://zkaan2xfbuxia2wpf7ofnkbz6r5zdbbvxbunvp5g2iebopbfc4iqmbad.onion/`

**NoLog.cz （様々なサービス）** \
`https://nolog.cz/`

**Redlib （Reddit の代替フロントエンド）** \
`https://github.com/redlib-org/redlib/`

**Riseup （様々なサービス）** \
`https://riseup.net/` \
`http://vww6ybal4bd7szmgncyruucpgfkqahzddi37ktceo3ah7ngmcopnpyyd.onion/`

**searXNG （メタ検索エンジン）のインスタンス** \
`https://searx.space/` \
`http://searxspbitokayvkhzhsnljde7rqmn7rvoga6e4waeub3h7ug3nghoad.onion/`

**SimpleLogin （メール転送）** \
`https://simplelogin.io/`

**Snopyta （様々なサービス）** \
`https://snopyta.org/`

**sourcehut （git リポジトリのホスティング）** \
`https://sourcehut.org/`

**TorZillaPrint （ブラウザ指紋テスト）** \
`https://arkenfox.github.io/TZP/tzp.html`

**Unshort.link （短縮 URL の拡張）** \
`https://unshort.nolog.cz/`

**URL Expander （短縮 URL の拡張）** \
`https://urlex.org/`

**Wayback Machine （ウェブアーカイブ）** \
`https://web.archive.org/`

**Whoogle （Google 検索の代替フロントエンド）** \
`https://github.com/benbusby/whoogle-search`

**Wikiless （Wikipedia の代替フロントエンド）** \
`https://github.com/Metastem/wikiless`

# 市民社会組織 {#civil-society-orgs}

**Access Now （INT）** \
`https://www.accessnow.org/`

**Amnesty International （INT）** \
`https://www.amnesty.org/`

**Citizen Lab （CA）** \
`https://citizenlab.ca/`

**EDRi （EU）** \
`https://edri.org/`

**Electronic Frontier Foundation （US）** \
`https://eff.org/`

**Electronic Privacy Information Center （US）** \
`https://epic.org/`

**Fight for the Future （US）** \
`https://www.fightforthefuture.org/`

**Front Line Defenders （IE）** \
`https://www.frontlinedefenders.org/` \
`http://3g2wfrenve2xcxiotthk4fcsnymzwfbttqbiwveoaox7wxkdh7voouqd.onion/`

**Global Encryption Coalition （INT）** \
`https://www.globalencryption.org/`

**Internet Freedom Foundation （IN）** \
`https://internetfreedom.in/`

**JCA-NET （JP）** \
`https://www.jca.apc.org/jca-net/`

**La Quadrature du Net （FR）** \
`https://www.laquadrature.net/`

**Privacy International （UK）** \
`https://privacyinternational.org/` \
`https://privacy2ws3ora5p4qpzptqr32qm54gf5ifyzvo5bhl7bb254c6nbiyd.onion/`


***

# アリスとボブ {#back}

ガイド一覧に戻る
--- [Markdown](../ja-md/alice2bob-ja-preface-2.4.md#list)
\| [HTML](../ja-html/alice2bob-ja-preface-2.4.html#list)

## 連絡先 {#back-contact}

### PGP 鍵 {#back-contact-PGP}

`1D3E 313C 4DB2 B3E0 8271 FC48 89BB 4CBB 9DBE 7F6D`

- ウェブサイト上：
  `https://git.disroot.org/alice2bob/alice2bob/src/branch/master/alice2bob.asc`
- 鍵サーバ（クリアネット）：
  `https://keys.openpgp.org/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D`
- 鍵サーバ（オニオン）：
  `http://zkaan2xfbuxia2wpf7ofnkbz6r5zdbbvxbunvp5g2iebopbfc4iqmbad.onion/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D`

### ウェブサイト {#back-contact-website}

<img class="qr" src="../assets/qr/contact-website.png" width="222" height="222" alt="ウェブサイト QR コード">

`https://git.disroot.org/alice2bob/alice2bob`

## 検証 {#back-verify}

`https://git.disroot.org/alice2bob/alice2bob/src/branch/master/verify.md`

## 免責事項 {#back-disclaimer}

アリスとボブはセキュリティ専門家でも法律家でも
金融アドバイザーでも医師でもありません。
本シリーズの目的はあくまでも情報提供で、何の助言でもありません。
本シリーズは全くの無保証で提供されます。
本シリーズによってどんな不利、損失または損害が生じても、
アリスとボブは一切の責任を負いません。
ご自身の状況を考慮し、自己責任で本シリーズを使ってください。
