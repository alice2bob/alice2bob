<img class="top-banner" src="../assets/img/top-banner.png" width="780" height="204" alt="アリスとボブのバナー">

# Tor Browser {#top}

アリスとボブ

バージョン： 2.4

本ガイドの最終更新： 2024-08-20

Tor Browser 13.5

ガイド一覧に戻る
--- [Markdown](../ja-md/alice2bob-ja-preface-2.4.md#list)
\| [HTML](../ja-html/alice2bob-ja-preface-2.4.html#list)

***

1. [Tor Browser とは](#what-is-Tor-Browser)
2. [特徴](#features)
3. [注意点と限界](#warnings-and-limitations)
   1. [Firefox ベースに関するセキュリティ問題](#Firefox-security-issues)
   2. [Tor Browser のブラウザ指紋](#Tor-Browser-browser-fingerprint)
   3. [ビューポートの縦と横の長さの調整を控える](#dont-adjust-viewport)
   4. [セキュリティとプライバシーの意識は依然として必要](#security-and-privacy-awareness)
   5. [非匿名化につながる恐れのある行動・現象に注意](#beware-of-deanonymization)
4. [準備](#prepare)
   1. [ダウンロード](#download)
   2. [検証](#verify)
   3. [インストール](#install)
5. [設定](#configure)
   1. [Tor への接続](#connecting-to-Tor)
   2. [ユーザインターフェイス](#user-interface)
   3. [セキュリティレベル](#security-level)
   4. [非暗号化接続の停止](#block-unencrypted-connections)
   5. [ブラウザの自動更新](#browser-auto-update)
   6. [Onion-Location](#Onion-Location)
   7. [他の設定](#other-settings)
   8. [拡張機能](#extensions)
6. [使い方](#how-to-use)
   1. [Tor 回線の表示と変更](#change-Tor-circuit)
   2. [記憶喪失と永続性](#amnesia-and-persistence)
   3. [Canvas データ抽出](#canvas-data-extraction)
   4. [非暗号化接続の例外的許可](#allow-unencrypted-exception)
   5. [セキュリティレベルの操作](#changing-the-security-level)
   6. [HTTP リファラ](#HTTP-referer)
   7. [ウェブサイトのセキュリティ対策との衝突](#website-security-features)
   8. [Tor ブロッキングの回避](#Tor-blocking-circumvention)
   9. [代替検索エンジン](#alternative-search-engines)

*注：本ガイドに記載した Tor Project のウェブページ
（...`torproject.org`...）への外部リンクは
オニオン接続でアクセスできます。
オニオンアドレスを得るには、以下のように URL を置き換えてください。*

- `https://support.torproject.org` →
  `http://rzuwtpc4wb3xdzrj3yeajsvm3fkq4vbeubm2tdxaqruzzzgs5dwemlad.onion`
- `https://tb-manual.torproject.org` →
  `http://dsbqrprgkqqifztta6h3w7i2htjhnq7d3qkh3c7gvc35e66rrcv66did.onion`
- `https://www.torproject.org` →
  `http://2gzyxa5ihm7nsggfxnu52rck2vv4rvmdlkiu3zzui5du4xyclen53wid.onion`

# Tor Browser とは {#what-is-Tor-Browser}

**Tor Browser** とは、
匿名性とプライバシー保護を目的に開発されたウェブブラウザです。
以下で構成されます。

- 設定済みの Tor クライアント。
- Mozilla Firefox （ブラウザのベース）。
- Firefox 設定変更と拡張機能：
  様々なブラウザ指紋抽出防止や非匿名化防止機能、一部のロギングの無効化、
  セキュリティレベル設定の追加、セキュリティ強化など。
- NoScript：
  JavaScript と XSS を制御する Firefox プラグイン。

# 特徴 {#features}

Tor Browser の特徴を簡潔に紹介します。
設定と使用に関連する Tor Browser の特徴を後で解説します。

- Tor への接続
  - 接続方法の設定（直接、ブリッジなど）
  - Tor 回路の表示と変更
- 3 段階のセキュリティレベル
- 非暗号化接続の扱い
  - HTTPS への切り替え
  - 自動停止と例外的許可
- 追跡防止、ブラウザ指紋抽出防止[^fingerprinting-protection]、
  非匿名化防止[^Tor-uplift]
  - ブラウザ指紋統一の追求
    （ユーザエージェント、時間帯、フォントセット、 OS など）
  - first-party isolation：
    サイト関連データ（クッキー、キャッシュなど）を
    サイト（ドメイン）別に分離する機能
  - HTML5 canvas 要素からのデータ抽出の遮断
  - ビューポートの縦と横の長さのレターボックス化（唯一性低減）
  - マウスの動きと打鍵の情報抽出に対する時間精度の低下（唯一性低減）
- ブラウザ終了時のセッションデータの消去（記憶喪失）
- Onion-Location：オニオン接続への切り替え

# 注意点と限界 {#warnings-and-limitations}

Tor Browser はプライバシーや匿名性を保証しません。
Tor 自体はプライバシーや匿名性を保証しない一方、
ウェブブラウザは攻撃対象領域の広い複雑なソフトウェアです。

本セクションでは、 Tor Browser についての
一部の注意点や限界について説明します。
以下に紹介する一部の点は Tor Browser だけでなく
他のウェブブラウザにもあてはまります。

→ ガイド：
**Tor 匿名化ネットワーク § 注意点と限界**
--- [Markdown](../ja-md/alice2bob-ja-Tor-2.4.md#warnings-and-limitations)
\| [HTML](../ja-html/alice2bob-ja-Tor-2.4.html#warnings-and-limitations)

## Firefox ベースに関するセキュリティ問題 {#Firefox-security-issues}

Tor Browser は Firefox をベースにしているため、
Firefox の多くのセキュリティ問題を受け継ぎます。
Firefox のセキュリティ（エクスプロイト保護）
についての批判が指摘されています。

→ 外部資料：
Firefox のセキュリティ問題：
`https://madaidans-insecurities.github.io/firefox-chromium.html`

→ 外部資料：
"Tor and its Discontents"：
`https://medium.com/@thegrugq/tor-and-its-discontents-ef5164845908`

## Tor Browser のブラウザ指紋 {#Tor-Browser-browser-fingerprint}

Tor Browser は、利用者が Tor Browser ならではの、
できる限り統一したブラウザ指紋をウェブサイトに見せようとします。
Tor Browser のブラウザ指紋は、
Firefox のブラウザ指紋に一部似ているものの、
他のブラウザのブラウザ指紋の真似をしたり
無作為にしたりしようとしません。

ちなみに、 Tor Browser 12.5 （デスクトップ版）の
ユーザエージェント文字列は以下のようなもので、
Windows 上の Firefox に該当するユーザエージェント文字列です。

`Mozilla/5.0 (Windows NT 10.0; rv:102.0) Gecko/20100101 Firefox/102.0`

Tor Browser 利用者が Tor Browser の設定を変更すると、
ブラウザ指紋が Tor Browser の標準のブラウザ指紋から離れて、
それによって匿名性を損なう恐れがあります。

## ビューポートの縦と横の長さの調整を控える {#dont-adjust-viewport}

Tor Browser の機能の一つは、
ビューポートの縦と横の長さのレターボックス化です。
Tor Browser のウィンドウの縦と横の長さを問わず、
**ビューポート**（ウェブページが表示される長方形）の縦と横の長さは
200px × 100px の倍数に縮まります。
この機能はビューポートの縦と横の長さに基づく
ブラウザ指紋唯一性を軽減する対策です。

**レターボックス化**（letterboxing）の機能によって
ビューポートの縦と横の長さは 200px × 100px の倍数に限られるが、
それでもビューポートの縦と横の長さの情報は
利用者の非匿名化につながる可能性が残ります。
Tor Browser のウィンドウの縦と横の長さの調整と
ウィンドウの最大化・フルスクリーン化を控えることを推奨します。

## セキュリティとプライバシーの意識は依然として必要 {#security-and-privacy-awareness}

匿名性とプライバシーに特化したブラウザを利用していても、
依然として日常的なセキュリティとプライバシーの意識は必要です。
以下にのブラウザの利用に関連する対策の具体例をあげます。

- 原則として情報提供をしないで、例外的かつ慎重に行う。
  - センシティブ情報の提供を必要最低限にする。
  - セキュリティやプライバシーを損なう情報を提供しない
    （例：暗号通貨のシード、秘密鍵、住所、自宅の鍵など）。
  - 要求に応じるべきでないか要求者が情報を知る必要がない場合、
    拒否・無視するか虚偽の情報を提供する。
- フィッシングや詐欺に用心する。
- リンクを無闇にクリックしない。
- URL の確認などして接続先を検証する。
- ダウンロードしたファイルのデータ完全性を検証する。
- インターネットに接続している時に
  Tor Browser でダウンロードしたファイルを開かない。
- 良好なパスワードを選び、適切な多要素認証でアカウントを保護する。
- あらゆる通信（ウェブトラフィックやメールなど）を暗号で保護する。
- できる限り JavaScript[^malicious-JavaScript] を無効化する。
- 代替検索エンジンを利用する。（例： DuckDuckGo、 searXNG）
- リンクをクリックする時の HTTP referer 発信に注意し、
  必要に応じて（URL のコピーペーストで）発信防止をする。

## 非匿名化につながる恐れのある行動・現象に注意 {#beware-of-deanonymization}

インターネット上でプライバシーと匿名性を維持することは簡単ではなく、
残念ながら訪問者に対する監視・追跡を実践しているウェブサイトが多いです。
Tor Browser は技術面で匿名性を保護する対策をなるべく取っているが、
それに加えて利用者も匿名性を保護する対策を取るといいでしょう。
特に、匿名性を必要とする利用者にとって必要です。

プライバシーと匿名性を維持する対策の具体例を以下にあげます。

- Tor Browser の無闇の設定の変更または拡張機能の追加をしない。
- ログインや名乗りなどの際、
  無関係のアカウント・身元との繋がりを生成しないよう用心する。
- 文章（コメント、投稿、コードなど）、メディア（画像、音声、動画など）や
  他の情報を提供する時にセンシティブ情報の自己晒しに注意する。
  - メタデータ（例：ファイル名、カメラの機種、位置情報、著者など）
  - 身元を特定する内容（例： PGP 公開鍵、顔や財産などが写っている写真）
  - 身元、場所、時間帯などを絞る内容（例：観光名所で撮った最近の写真）
  - 文章の著者を絞る手口となる表現スタイル（stylometry）
- ウェブサイト訪問時の唯一性のある行動的情報の生成を
  できる限り避けるかごまかす。
  - 訪問の時刻とその一連のパターン
  - 検索欄やフォームなどの入力情報
  - ページ訪問履歴
  - マウスの移動とクリック
  - Lazy loading[^lazy-loading] によって発信される
    スクロールの位置や速度の情報
- 打鍵、マウス、スクロールなど、生体やデバイスなどの
  唯一特徴の抽出を防ぐ。
  - マウスウィールまたはマウスより、
    <kbd>Page Up</kbd> と <kbd>Page Down</kbd>
    でスクロールするとよりいいかもしれない。
  - フォームの欄に直接入力せず、
    外部のテキストエディタから内容をコピーペーストする。

→ 外部資料：
ブラウザ指紋抽出攻撃：
`https://blog.torproject.org/browser-fingerprinting-introduction-and-challenges-ahead/`

# 準備 {#prepare}

## ダウンロード {#download}

Tor Browser は Tor Project のサイトからダウンロードできます。
Tor Browser 自体と共に、対応する PGP 署名もダウンロードできます。

`https://www.torproject.org/download/`

対応 OS は以下のとおりです。

- Android （AArch64、 ARM、 x86\_64、 x86）： `.apk` ファイル
- Linux （x86\_64）： `.tar.xz` ファイル
- macOS （x86\_64）： `.dmg` ファイル
- Windows （x86\_64）： `.exe` ファイル

Android では Google Play や F-Droid からもダウンロード可能です。

Apple は iOS 用ウェブブラウザの開発を制限するため、
iOS 版の Tor Browser は存在しません。
iOS で匿名ウェブ閲覧をしたい場合、
Onion Browser を検討してください。

`https://onionbrowser.com/`

### Tor Project ウェブサイトブロッキングの回避 {#GetTor}

Tor Project のウェブサイトにアクセスできない場合、
以下のミラーで Tor Browser のダウンロードを試行してください。

- `https://tor.eff.org/`
- `https://tor.ccc.de/`
- `https://tor.calyxinstitute.org/`

また、 GetTor というサービスで
Tor Browser を入手できるかもしれません。

1. メールアドレス `gettor@torproject.org` 宛に
   希望の OS （と言語）を本文に書かれたメールを送る。
   （例： "Linux"、 "macOS es"、 "Windows ja"）
2. GetTor からの返答からリンクを入手する。
3. リンクを用いて Tor Browser をダウンロードし、
   ダウンロードしたファイルを[検証](#verify)する。

→ 外部資料：
GetTor と検閲：
`https://support.torproject.org/censorship/`

## 検証 {#verify}

入手した Tor Browser が安全で、
実際に Tor Project がリリースしたインストールファイルだと
検証することが重要です。
Tor Browser の検証方法と PGP 鍵の入手などについて、
Tor Project の説明ページを参照してください。

→ 外部資料：
署名検証方法：
`https://support.torproject.org/tbb/how-to-verify-signature/`

1. Tor Browser 開発者の署名用 PGP 公開鍵を入手する。
2. 公開鍵を（指紋を照合して）検証した上で、インポートする。
3. ダウンロードした Tor Browser ファイルを検証する。

まず、 Tor Browser 開発者の署名用 PGP 公開鍵を入手します。
以下の URL からダウンロードするか、 GnuPG で公開鍵を検索します。

`https://keys.openpgp.org/search?q=torbrowser%40torproject.org`

~~~
$ gpg --keyserver keys.openpgp.org --search-keys torbrowser@torproject.org
~~~

以上の GnuPG コマンド以外の方法で公開鍵を入手した場合、
公開鍵 `<key-file>` の指紋が正しいことを確認した上で
（上の検証方法のリンクを参照）、
公開鍵を GnuPG の鍵輪に追加します。

~~~
$ gpg --show-key <key-file>
...（鍵の情報の表示）
$ gpg --import <key-file>
~~~

公開鍵のインポートが成功したことを確認します。

~~~
$ gpg -k torbrowser@torproject.org
...（鍵の情報の表示）
~~~

ダウンロードしたファイル `<file>`
（例： `tor-browser-linux-x86_64-13.5.2.tar.xz`）を
署名ファイル `<signature>`
（例： `tor-browser-linux-x86_64-13.5.2.tar.xz.sig`）で
検証します。

~~~
$ gpg --verify <signature> <file>
~~~

検証コマンドの出力を確認します。
`Good signature from "Tor Browser Developers`...
（またはそれに類似した日本語）が表示された場合、
入手した Tor Browser のファイルが安全な可能性が高いです。

## インストール {#install}

Tor Project のページにインストール方法の説明があるが、
ここで簡潔に説明します。

→ 外部資料：
Tor Browser のインストール：
`https://tb-manual.torproject.org/installation/`

- Android：入手・検証した `.apk` ファイルを実行して、
  「インストール」または "Install" で進みます。
- macOS：入手・検証した `.dmg` ファイルを実行して、手順を踏みます。
- Windows：入手・検証した `.exe` ファイルを実行して、手順を踏みます。

Linux のインストール方法には説明が必要です。

1. 希望のインストール場所 `<directory>` へ作業ディレクトリを変更する。
2. 入手・検証した `.tar.xz` ファイル `<file>` を解凍する。
3. 新しく現れた Tor Browser へ作業ディレクトリを変更する。
4. インストール・起動スクリプトを実行する。

~~~
$ cd <directory>
$ tar -xf <file>
$ cd tor-browser/
$ ./start-tor-browser.desktop
~~~

*注：上の `./start-tor-browser.desktop` コマンドが失敗した場合、
ファイルの実行パーミッションが設定されていないかもしれません。
`chmod +x start-tor-browser.desktop` コマンドで
実行パーミッションを追加した上で再度試行してください。*

# 設定 {#configure}

Tor Browser は利用者が設定を変更せずに
使用できるように開発されたブラウザです。
多くの設定を調整しなければ適切に動作しない
Mozilla Firefox と異なって、
ほとんどの初期設定はプライバシーを重視する利用者のために
相応しく設定されています。
逆に、 Tor Browser の設定を不慎重に変更すると
ブラウザの振る舞いが標準から離れて、
匿名性が低下してしまう恐れがあります。
匿名性を維持するために、
Tor Browser の設定変更を最低限にすることが重要です。

しかし、 Tor Browser を使用する前に
一部の設定を変更することを推奨します。
例えば、 Tor Browser の初期設定では
セキュリティレベルが "Standard" （標準）となっています。

本セクションでは Tor に接続する前に
変更・確認を推奨する設定を解説します。
デスクトップ・Linux 版を前提に、設定を説明します。
他の版では詳細が異なるかもしれません。

## Tor への接続 {#connecting-to-Tor}

<img class="display" src="../assets/img/Tor-Browser/connect-to-Tor-prompt.png" width="754" height="180" alt="Tor 接続のメニュー">

Tor ネットワークへの自動接続
"Always connect automatically" を推奨しません。
例えば、 Tor Browser 内のブックマークを見るためだけ
Tor Browser を起動させた場合、
または、ブリッジなどの接続設定の変更が必要な場合、
Tor ネットワークへの自動的な接続は望ましくありません。

すぐに Tor に接続して Tor Browser を使用したい場合、
"Connect" ボタンをクリックします。

通常どおりに Tor に接続できないか
Tor ブリッジを使用したい場合、
"Configure Connection..." をクリックします。

接続設定はいつでも URL `about:preferences#tor`
でアクセスできます。

→ 外部資料：
Tor への接続：
`https://support.torproject.org/connecting/`

### Tor ブリッジの設定 {#configure-Tor-bridge}

<img class="display" src="../assets/img/Tor-Browser/connect-to-Tor-bridges.png" width="672" height="519" alt="Tor ブリッジの設定">

ここで Tor Browser の Tor ブリッジの設定方法を説明します。

Tor Browser のユーザインターフェイスでは
以下のようにブリッジを設定できます。

- "Select a built-in bridge..."：
  内蔵ブリッジを選択する。
- "Add new bridges..."：
  入手したブリッジを入力する。
- "Request bridges..."：
  Tor Project にブリッジを要求する。

→ ガイド：
**Tor § Tor ブリッジ**
--- [Markdown](../ja-md/alice2bob-ja-Tor-2.4.md#Tor-bridges)
\| [HTML](../ja-html/alice2bob-ja-Tor-2.4.html#Tor-bridges)

### プロキシとファイアウォール通過の設定 {#configure-advanced-connection}

<img class="display" src="../assets/img/Tor-Browser/connect-to-Tor-advanced-0.png" width="672" height="117" alt="詳細設定">
<img class="display" src="../assets/img/Tor-Browser/connect-to-Tor-advanced-1.png" width="762" height="636" alt="プロキシとファイアウォール通過の設定">

プロキシを用いてインターネットに接続するか
ファイアウォールを通過する必要がある場合、
"Advanced" 見出しの下にある欄にプロキシを指定したり
許可されているポート番号を指定したりします。

- URL `about:preferences#connection` →
  "Advanced" →
  "Settings..."

### ログを表示 {#view-logs}

Tor Browser のログを表示したい場合、
"Advanced" 見出しの下にある "View Log..." を選択します。

## ユーザインターフェイス {#user-interface}

Tor Browser の現在の状態を一瞬で見えたり調整したりできるよう、
ユーザインターフェイス（GUI）を調整した方がいいでしょう。

- セキュリティレベル
  <img src="../assets/img/Tor-Browser/security-level-icon-standard.png" width="20" height="20" alt="アイコン">
- NoScript の動作状態
  <img src="../assets/img/Tor-Browser/NoScript-icon-questionmark.png" width="20" height="20" alt="アイコン">

<img class="display" src="../assets/img/Tor-Browser/settings-popup-more-tools.png" width="396" height="717" alt="3 つの横線のアイコンの設定メニュー">
<img class="display" src="../assets/img/Tor-Browser/settings-popup-customize.png" width="396" height="439" alt="More tools 内の設定メニュー">

ユーザインターフェイスの設定は以下のようにアクセスします。

- 3 本の横線のアイコン
  <img src="../assets/img/Tor-Browser/settings-icon-three-horizontal-bars.png" width="20" height="20" alt="アイコン">
  （URL バーの右にあるはず） →
  "More tools" →
  "Customize toolbar..."

ドラッグとドロップでユーザインタフェイスを自由に調整できます。
上に見える 3 つのアイコンを URL バーの左または右に
好きなように移動させます。

## セキュリティレベル {#security-level}

Tor Browser には三段階のセキュリティレベルがあります。

- **Standard** （標準）
  <img src="../assets/img/Tor-Browser/security-level-icon-standard.png" width="20" height="20" alt="アイコン">：
  Tor Browser とウェブサイトの全ての機能が有効化。
- **Safer** （より安全）
  <img src="../assets/img/Tor-Browser/security-level-icon-safer.png" width="20" height="20" alt="アイコン">：
  ウェブサイトのより危険な機能を無効化。
  非暗号化接続のウェブサイトの JavaScript が無効化。
  一部のフォントと数学記号が無効化。
- **Safest** （最も安全）
  <img src="../assets/img/Tor-Browser/security-level-icon-safest.png" width="20" height="20" alt="アイコン">：
  静的なウェブサイトと基本的なサービスに必要な機能だけを許可。
  全てのウェブサイトで JavaScript が無効化。
  Safer 設定に加えて、一部のアイコンと画像も無効化。

Safest 設定ではプライバシーと匿名性を損なう多くの機能が無効になります。
例えば、 JavaScript が完全に無効化されます。

<img class="display" src="../assets/img/Tor-Browser/security-level-popup-safest.png" width="475" height="325" alt="セキュリティレベルのポプアップ">

セキュリティレベルの設定は以下のようにアクセスできます。

- GUI の盾のアイコン → "Change..."
- URL `about:preferences#privacy` → "Security Level"

<img class="display" src="../assets/img/Tor-Browser/security-level-setting-safest.png" width="667" height="462" alt="セキュリティレベルのポプアップ">

## 非暗号化接続の停止 {#block-unencrypted-connections}

**非暗号化接続**（HTTP）でデータを送受信すると傍受される恐れがあります。
また、非暗号化接続でデータの改竄やマルウェア注入も可能です。
HTTPS-Only という設定で、このような非暗号化接続の扱いを制御します。

<img class="display" src="../assets/img/Tor-Browser/HTTPS-Only-setting.png" width="672" height="213" alt="HTTPS-Only 設定">

非暗号化接続の停止を有効化するには、 HTTPS-Only 設定の
"Enable HTTPS-Only Mode in all windows" を選択します。

- URL `about:preferences#privacy` → "HTTPS-Only Mode"

→ 外部資料：
HTTPS：
`https://support.torproject.org/https/`

## ブラウザの自動更新 {#browser-auto-update}

ソフトウェアを迅速に更新する重要性は高いが、
更新を実行する前にソフトウェア更新を検証するといいでしょう。
しかし、自動更新だとブラウザは勝手に更新をインストールします。

自動更新を無効化するには、以下のように設定を変更します。

- URL `about:preferences#general` → "Tor Browser Updates"

<img class="display" src="../assets/img/Tor-Browser/browser-update-setting-ask.png" width="672" height="144" alt="ブラウザの自動更新の設定">

"Check for updates but let you choose to install them" を選ぶと、
Tor Browser は新たな更新があるか確認するが、
更新を実行する前に利用者からの許可を要求します。

<img class="display" src="../assets/img/Tor-Browser/browser-update-popup.png" width="569" height="181" alt="ブラウザの更新の許可を要求するポプアップ">

新たな更新が発見され、ポプアップが表示されたら、
"Download" を選ぶことですぐに更新をダウンロードするか、
"Dismiss" を選ぶことで更新を延期します。

## Onion-Location {#Onion-Location}

<img class="display" src="../assets/img/Tor-Browser/Onion-Location-available-button.png" width="520" height="40" alt="URL バーに表示中の .onion available ボタン">

**Onion-Location** とは、
ウェブサイトが自分のオニオンアドレスを告知することで
Tor 利用者にオニオン接続をしてもうらう機能です。
ウェブサイトは以下のいずれかの方法で
オニオンアドレス `<address>` を告示します。

- HTTP 応答ヘッダ：
  `Onion-Location: <address>`
- HTML 属性：
  `<meta http-equiv="onion-location" content="<address>" />`

オニオンアドレスを告知するウェブサイトに
クリアネット HTTPS 接続で訪問する際、
設定によって Tor Browser は以下のいずれかをします。

- 自動的にオニオン接続に切り替える。
- ".onion available" という紫色のボタンを URL バーに表示する。

自動的にオニオン接続に切り替わる場合、
オニオンアドレスを所有するウェブサイトにクリアネット接続できません。
自動切り替えを無効化するには、
以下のように Onion-Location の設定を変えます。

- URL `about:config` →
  `privacy.prioritizeonions.enabled` を `true` から `false` に変更

## 他の設定 {#other-settings}

他に変更できる設定があります。
以下の設定を好みに変更してもセキュリティ、
ブラウザ指紋の唯一化、非匿名化などのリスクが
低いかもしれない設定の例です。

- デフォルトブラウザの確認
- 検索エンジンのリストとデフォルト検索エンジン
- <kbd>Ctrl</kbd>+<kbd>Tab</kbd> タブ切り替えの振る舞い
- ブラウザ起動後の初めのページ
- 新規タブのページ
- URL の表示とアドレス選択の振る舞い
- URL 入力時の（履歴、ブックマークなどに基づく）提案
- リンクを新規タブに開くか新規ウィンドウに開くかの振る舞い
- ダウンロードの保存先に関する振る舞い

セキュリティまたは非匿名化のリスクに
影響を及ぼすかもしれない設定は少なくないため、
慎重に設定を変更する必要があります。

## 拡張機能 {#extensions}

Chrome や Firefox など現代ブラウザは
アドオン、プラグイン、テーマなどの
拡張機能のインストールに対応しています。
しかし、これらの拡張機能を
Tor Browser に追加しないことを推奨します。
セキュリティ低下、ブラウザ指紋の唯一化、
非匿名化などの原因になる恐れがあります。

# 使い方 {#how-to-use}

本セクションでは Tor Browser の使い方について説明します。

## Tor 回線の表示と変更 {#change-Tor-circuit}

<img class="display" src="../assets/img/Tor-Browser/URL-popup-onion.png" width="492" height="446" alt="Tor 回路（オニオン接続の場合）">

Tor Browser では、ウェブサイトに安全に接続している場合、
URL バーの左側にいずれかのアイコンが表示されます。

- 普通の南京錠（HTTPS 接続）
- 円いオニオン形の南京錠（オニオン接続）

そのアイコンをクリックすると、
URL バーに表示されているサイトの Tor 回路が表示されます。

Tor ネットワークからの接続または
同じ IP アドレス（例えば出口ノード）からの多数の接続に対して、
接続またはサービス提供を拒否したり速度制限をしたりする
ウェブサイトは少なくありません。
ある接続に対して制限をかける判断方法は、
国または特定の IP アドレスのブラックリストまたはホワイトリスト、
特定のポート番号のブラックリストまたはホワイトリスト、
ブラウザ指紋、 JavaScript が有効か無効かなど、様々です。

このような制限は Tor 回線を変更するだけで回避できるかもしれません。
ただし、 Tor ネットワークのノードのリストは公開情報で、
Tor が利用するポート番号がだいたい決まっているため、
ウェブサイトは Tor ネットワークを
簡単かつ効果的にブロックすることができます。

また、現在使用中の出口ノードに何かしらの問題があった場合に
Tor 回線を変更すると解決できるかもしれません。

サイトの Tor 回路を変更するには、
"New Tor circuit for this site" をクリックします。
1 回変更しただけで制限を回避できるとは限らず、
アクセスが成功するまで何度も Tor 回線を
変更する必要があるかもしれません。

## 記憶喪失と永続性 {#amnesia-and-persistence}

Tor Browser が終了すると、
利用者のセッション情報は原則としてデバイスに残りません。

同じ Tor Browser のセッションを長時間使用するより、
ブラウザを定期的に再起動させたり
異なる活動に切り替える時に（例えば買い物から仕事へ）
再起動させたりすることを推奨します。

終了後に残らない情報は以下です。
（完全なリストではありません）

- キャッシュ
- クッキー（認証用、追跡用など）
- パーミッション（カメラ、マイク、 HTML5 canvas データ抽出など）
  （ただし、一部のパーミッションをデフォルトで
  自動的に許可または拒否できます）
- 履歴（閲覧、ダウンロード、検索、フォーム入力など）
- 一部の設定（例：ウィンドウサイズ）（次回にデフォルトに戻ります）

終了後に永続する情報は以下です。
（完全なリストではありません）

- ブックマーク
- ダウンロードしたファイル（ファイルシステムに残ります）
- 最後にダウンロードしたファイルの親ディレクトリ
- ほとんどの設定（接続、ユーザインターフェイス、セキュリティレベル、
  検索エンジンなど）

## Canvas データ抽出 {#canvas-data-extraction}

**Canvas 要素**（canvas element）とは、
ウェブページに 2 次元ビットマップ画像を描くための HTML5 要素で、
HTML5 に導入されたものです。
Canvas 要素自体はあくまでコンテナだけです。
画像の描画とデータ抽出は JavaScript で行うことを前提にした要素です。
Tor Browser では、 canvas 要素データ抽出の試みがあった時に
URL バーの左側にアイコン<img src="../assets/img/Tor-Browser/canvas-icon.png" width="20" height="20" alt="アイコン">が表示されます。

ウェブサイトはこの要素を用いて利用者に対する
ブラウザ指紋抽出攻撃ができます。
訪問者に見えない canvas 要素をウェブページに追加して、
その中にテキストや他の描画オブジェクトを挿入し、
結果の画像データを抽出・解析することで、
訪問者を特定するブラウザ指紋を
（訪問者が知らずに無断で）得ることができます。
デバイスとブラウザによって異なるフォントセット、
設定、ハードウェアなどがあるため、
この方法では訪問者を特定する唯一性の高い指紋が得られます。

→ 外部資料：
Canvas データ抽出：
`https://www.propublica.org/article/meet-the-online-tracking-device-that-is-virtually-impossible-to-block`

<img class="display" src="../assets/img/Tor-Browser/canvas-popup.png" width="551" height="222" alt="canvas データ抽出に関するポプアップ">

Canvas データ抽出の試みがあった場合、
Tor Browser はそのことを利用者にアイコンの表示で通知し、
データ抽出を許可するか拒否するかを聞きます。
ポプアップの "Block" をクリックすることでデータ抽出を拒否します。

残念ながら Tor Browser では永続的にパーミッションを拒否したり
全てのウェブサイトに及んで拒否したりできないみたいです。
そのため、通知された度に拒否する必要があります。

JavaScript が無効の場合に描画とデータ抽出が不可能なため、
Safest セキュリティレベルで Tor Browser を使うことも効果的です。

## 非暗号化接続の例外的許可 {#allow-unencrypted-exception}

[「非暗号化接続の停止」](#block-unencrypted-connections)では、
非暗号化接続を自動的に停止させる設定の有効化について説明しました。
しかし、たまに非暗号化接続を一時的にまたは永続的に許可する必要が
出てくるかもしれません。

<img class="display" src="../assets/img/Tor-Browser/HTTPS-Only-warning.png" width="866" height="412" alt="HTTPS-Only の注意表示">

サイトへの非暗号化接続を一時的に許可するには
“Continue to HTTP Site” をクリックします。
ブラウザ終了後にそのサイトを許可した情報が消えます。

非暗号化接続を許可する前に、 JavaScript が無効になるよう
セキュリティレベルを Safest に設定することを推奨します。

## セキュリティレベルの操作 {#changing-the-security-level}

[「セキュリティレベル」](#security-level)では、
セキュリティレベルの設定方法について説明しました。

セキュリティレベルについて、以下の使用方法を推奨します。

- 原則として Safest の設定で Tor Browser を使用する。
- 非暗号化接続でサイトにアクセスする時に、
  絶対に Safest の設定でアクセスする。
- Safest の設定で動作しない機能（例えば JavaScript）
  を有効化する必要がある場合、
  サイトにアクセスする重要性を評価し、
  サイトへのアクセスを断念するか、
  一時的に設定を Safer に引き下げた上でサイトにアクセスする。
- Safer の設定でアクセスする必要のあるサイトが正常に動作しない場合、
  一時的に設定を Standard に引き下げた上でサイトにアクセスする。
- Safer または Standard の設定が不要になったら、
  Safest に設定を戻す。

しかし、厳密にはセキュリティレベルは全ての開いているタブに適用され、
複数のタブが開いているとセキュリティレベルの操作が困難です。

ある JavaScript を必要とするサイト（サイト A）を
Safer の設定でアクセスした後に、
他のタブで他のサイトにアクセスするために
セキュリティレベルを Safest に引き上げた場合、
サイト A が正常に動作し続けるか動作しなくなるかはサイト次第です。
また、サイト A のタブを Safest の設定で（誤って）再読み込みしたら
サイト A が動作しなくなります。

一方、悪質な JavaScript のあるサイト（サイト B）を
Safest の設定でアクセスした後に、
他のタブで JavaScript と必要とするサイトにアクセスするために
セキュリティレベルを Safer に引き上げた場合、
サイト B は JavaScript が実行されないまま安全に閲覧できます。
しかし、サイト B のタブを Safer の設定で（誤って）再読み込みしたら
サイト B の悪質な JavaScript が実行されてしまいます。

以上のように、セキュリティレベルを操作しながらサイトにアクセスできるが、
気をつけながらセキュリティレベルとタブを操作する必要があります。

## HTTP リファラ {#HTTP-referer}

**HTTP リファラ**（HTTP referer）は HTTP 要求ヘッダの属性で、
ウェブサイト訪問者があるウェブページを要求する時に
ウェブサイトに送られるリンク元の URL を指します。
ウェブサイトはこの情報を入手することで
訪問者がどこから要求されたページに辿り着いたかを把握でき、
ウェブサイトの最適化やセキュリティ向上などに利用できます。

HTTP リファラ `<referer-URL>` は
以下の例のように HTTP 要求に含まれます。

~~~
GET /download HTTP/2
Host: www.torproject.org
... （他の属性を省略）
Referer: <referer-URL>
...
~~~

このように HTTP リファラが明かされることには
セキュリティとプライバシーの問題があります。
リンク元の URL 自体にセンシティブ情報
（例：身元を特定する情報、パスワード）が含まれているか
リンク元を秘密にしたい場合、
HTTP リファラが送られると問題になります。

- 例 1 ：
  `https://mail.nonexistmail.com/a2DFc5Qq09ol/inbox/`
- 例 2 ：
  `https://forum.supersecret.xyz/mutual-aid/alcohol/`

HTTP リファラの発信を防止するには、
閲覧しているページ内のリンクをクリックして開かないことです。
クリックする代わりに、以下のような手順で
アクセスしたいリンクのコピーペーストをします。

1. アクセスしたいリンクのコンテキストメニューを開く。
   - リンクをフォーカスにして <kbd>Menu</kbd> キーを押す。
   - リンクを**右クリック**する。
2. "Copy Link" （または同様のオプション）を選ぶ。
3. 新規タブを開く（<kbd>Ctrl</kbd>+<kbd>T</kbd>）。
4. URL バーにコピーした URL をペースト
   （<kbd>Ctrl</kbd>+<kbd>V</kbd>）して開く（<kbd>Enter</kbd>）。

なお、リンク元がオニオンアドレスの場合、
HTTP リファラがリンク先に送られないよう
Tor Browser に設定されています[^onion-referer]。

## ウェブサイトのセキュリティ対策との衝突 {#website-security-features}

サービス自体や利用者などを保護するためのセキュリティ対策
を取っているウェブサイトは少なくありません。
接続の IP アドレス、ブラウザ指紋、打鍵、マウスの振る舞いなどを監視し、
想定した必要に応じて不審な接続を拒否したり追加認証を求めたりします。

Tor Browser を利用すると、あるウェブサイトに接続する度に
毎回異なる出口ノード（したがって IP アドレス）を用いて接続します。
また、 Tor ネットワークからの接続を不審扱いするウェブサイトもあります。
そのためか、 Tor 利用者は追加認証や
CAPTCHA[^CAPTCHA] などの要求に遭いがちです。

あるウェブサイトに関連するアカウントに多要素認証を有効化すると、
そのウェブサイトへのログインがより簡単になるかもしれません。
自分にとって適切かつ安全な多要素認証を有効化すると、
アカウントのセキュリティ向上にもなるため、
多要素認証の可能な限りの使用を推奨します。
ただし、 SMS や 電子メールなどでの多要素認証方式は
安全でないため使わない方がいいかもしれません。

## Tor ブロッキングの回避 {#Tor-blocking-circumvention}

[Tor 回路の変更](#change-Tor-circuit)について前述しました。
しかし、それを繰り返しても失敗となるウェブサイトがあります。
その場合、以下の方法でウェブサイトのコンテンツを
閲覧できるかもしれません。

- 検索エンジンが保存したキャッシュページ
- ウェブアーカイブ
  - **Wayback Machine**：
    `https://web.archive.org/`
- 代替フロントエンド[^front-end]
  - **Invidious** （Youtube の代替）
    - ウェブサイト：
      `https://invidious.io/`
    - インスタンス一覧：
      `https://github.com/iv-org/documentation/blob/master/invidious-instances.md`

Wayback のアーカイブナビゲータは JavaScript を要するが、
希望のアーカイブを閲覧する前に、 JavaScript を無効化して閲覧できます
（ウェブサイト次第です）。

代替フロントエンドは、もとのサービスと違って JavaScript を要しないが、
一部の機能を使用するには JavaScript を有効化する必要があります。

## 代替検索エンジン {#alternative-search-engines}

大量監視を犯している Bing、 Facebook、 Google、 Yahoo などを避けて、
様々な代替検索エンジンを利用することを推奨します。

- **Brave**
  - クリアネット：
    `https://search.brave.com/`
  - オニオンサービス：
    `https://search.brave4u7jddbv7cyviptqjc7jusxh72uik7zt6adtckl5f4nwy2v72qd.onion/`
- **DuckDuckGo**
  - クリアネット：
    `https://duckduckgo.com/`
  - オニオンサービス：
    `https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion/`
- **searXNG**
  - ウェブサイト（クリアネット）：
    `https://searx.space/`
  - ウェブサイト（オニオンサービス）：
    `http://searxspbitokayvkhzhsnljde7rqmn7rvoga6e4waeub3h7ug3nghoad.onion/`
- **Whoogle** （Google 検索の代替フロントエンド）
  - `https://github.com/benbusby/whoogle-search`

見つけた検索エンジンのブックマークを作成したり
検索エンジンを Tor Browser に追加すると良いでしょう。

Tor Browser の検索エンジン設定には以下のようにアクセスします。

- 検索バーの虫眼鏡アイコン → 歯車（設定）アイコン
- URL `about:preferences#search`

"Default Search Engine" 見出しの下のドロップダウンメニューで
デフォルトに利用する検索エンジンを選びます。
しかし、これを効果的に利用するには、
事前に希望の検索エンジンを Tor Browser に追加する必要があります。

"Search Shortcuts" 見出しの下のリストは
Tor Browser に追加された検索エンジンです。
ここで検索エンジンの順番を調整したり不要なものを削除したりできます。



[^CAPTCHA]: CAPTCHA （キャプチャ）とは、
応答者がコンピュータでなく人間であることを確認するための
チャレンジレスポンス認証です。
変形した英数字の認識、写真に写っている物体の認識、音声認識など
コンピュータにとって困難な作業が認証の主な形式です。

[^fingerprinting-protection]: ブラウザ指紋抽出防止機能は
Firefox にも入っています。
`https://support.mozilla.org/ja/kb/firefox-protection-against-fingerprinting`

[^front-end]: フロントエンド（front-end）とは、
ウェブサイトやアプリケーションのユーザインタフェイスのことで、
利用者との相互作用をする部分を意味します。
データを処理・保存するバックエンド（back-end）の対照となる部分です。

[^lazy-loading]: Lazy loading とは、
ウェブページ内の全ての要素（主に画像）をすぐに読み込むことと対照し、
ブラウザのビューポートに表示されそうになる時に要素を読み込む方式です。
ブラウザ利用者に関連するスクロールの移動
（したがって訪問者が気になる内容または訪問者のスクロール移動の特徴）
の追跡手段としてウェブサイトが利用できるため、
プライバシーと匿名性を損なう可能性があります。

[^malicious-JavaScript]: 残念ながら現代のほとんどのウェブサイトには
ブラウザ指紋抽出、追跡、行動解析など悪質な JavaScript が使われています。

[^onion-referer]: `about:config` 内の設定：
`network.http.referer.hideOnionSource: true`

[^Tor-uplift]: Mozila と Tor Project との連携によって、
Tor Browser のプライバシー機能（Firefox を変更するパッチ）
が Firefox に持ち込まれています。
`https://wiki.mozilla.org/Security/Tor_Uplift`


***

# アリスとボブ {#back}

ガイド一覧に戻る
--- [Markdown](../ja-md/alice2bob-ja-preface-2.4.md#list)
\| [HTML](../ja-html/alice2bob-ja-preface-2.4.html#list)

## 連絡先 {#back-contact}

### PGP 鍵 {#back-contact-PGP}

`1D3E 313C 4DB2 B3E0 8271 FC48 89BB 4CBB 9DBE 7F6D`

- ウェブサイト上：
  `https://git.disroot.org/alice2bob/alice2bob/src/branch/master/alice2bob.asc`
- 鍵サーバ（クリアネット）：
  `https://keys.openpgp.org/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D`
- 鍵サーバ（オニオン）：
  `http://zkaan2xfbuxia2wpf7ofnkbz6r5zdbbvxbunvp5g2iebopbfc4iqmbad.onion/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D`

### ウェブサイト {#back-contact-website}

<img class="qr" src="../assets/qr/contact-website.png" width="222" height="222" alt="ウェブサイト QR コード">

`https://git.disroot.org/alice2bob/alice2bob`

## 検証 {#back-verify}

`https://git.disroot.org/alice2bob/alice2bob/src/branch/master/verify.md`

## 免責事項 {#back-disclaimer}

アリスとボブはセキュリティ専門家でも法律家でも
金融アドバイザーでも医師でもありません。
本シリーズの目的はあくまでも情報提供で、何の助言でもありません。
本シリーズは全くの無保証で提供されます。
本シリーズによってどんな不利、損失または損害が生じても、
アリスとボブは一切の責任を負いません。
ご自身の状況を考慮し、自己責任で本シリーズを使ってください。
