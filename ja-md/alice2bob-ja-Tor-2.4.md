<img class="top-banner" src="../assets/img/top-banner.png" width="780" height="204" alt="アリスとボブのバナー">

# Tor 匿名化ネットワーク {#top}

アリスとボブ

バージョン： 2.4

本ガイドの最終更新： 2024-02-05

OnionShare 2.4 \| Tor 0.4.6

ガイド一覧に戻る
--- [Markdown](../ja-md/alice2bob-ja-preface-2.4.md#list)
\| [HTML](../ja-html/alice2bob-ja-preface-2.4.html#list)

***

1. [Tor とは](#what-is-Tor)
2. [概要](#overview)
   1. [オニオンルーティング](#onion-routing)
   2. [ネットワーク通信の匿名化](#network-anonymity)
   3. [検閲と差別の回避](#censorship-resistance)
   4. [オニオンサービス](#onion-services)
3. [注意点と限界](#warnings-and-limitations)
   1. [Tor の匿名性集合](#anonymity-set)
   2. [レイテンシ増大とスループット低下](#latency-and-throughput)
   3. [TCP/IP 以外のトラフィックを中継できない](#TCP-IP-only)
   4. [出口ノードから通信を保護しない](#exit-node-problems)
   5. [アプリケーションによる情報漏洩](#leakage-by-applications)
   6. [利用者の行動による情報漏洩](#user-mistakes)
   7. [Tor を使用していることが明確](#Tor-doesnt-hide-Tor-use)
   8. [Tor に対する攻撃](#attacks-against-Tor)
   9. [悪質または危殆化したデバイス](#bad-device)
4. [アプリケーション](#applications)
   1. [一般 Tor クライアント `tor`](#general-Tor-client)
   2. [apt-transport-tor](#apt-transport-tor)
   3. [OnionShare](#OnionShare)
   4. [Tor Browser](#Tor-Browser)
   5. [Torsocks](#Torsocks)
5. [Tor ブリッジ](#Tor-bridges)
   1. [Pluggable transport](#pluggable-transports)
   2. [ブリッジの入手](#get-bridges)
   3. [ブリッジの追加](#add-bridges)
6. [Tor と VPN](#Tor-and-VPN)
   1. [VPN](#VPN)
   2. [Tor と VPN の併用](#Tor-and-VPN-together)
7. [おまけ：他の匿名化ネットワーク](#other-anonymization-networks)

*注：本ガイドに記載した Tor Project のウェブページ
（...`torproject.org`...）への外部リンクは
オニオン接続でアクセスできます。
オニオンアドレスを得るには、以下のように URL を置き換えてください。*

- `https://community.torproject.org` →
  `http://xmrhfasfg5suueegrnc4gsgyi2tyclcy5oz7f5drnrodmdtob6t2ioyd.onion`
- `https://gitlab.torproject.org` →
  `http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion`
- `https://snowflake.torproject.org` →
  `http://oljlphash3bpqtrvqpr5gwzrhroziw4mddidi5d2qa4qjejcbrmoypqd.onion`
- `https://support.torproject.org` →
  `http://rzuwtpc4wb3xdzrj3yeajsvm3fkq4vbeubm2tdxaqruzzzgs5dwemlad.onion`
- `https://tb-manual.torproject.org` →
  `http://dsbqrprgkqqifztta6h3w7i2htjhnq7d3qkh3c7gvc35e66rrcv66did.onion`

# Tor とは {#what-is-Tor}

**Tor** （トーア）とは、通常（直接的接続）より高いプライバシー・匿名性で
インターネットを利用することを可能にする技術です。
"Tor" という単語は以下の 3 つの意味があります。

- **Tor ネットワーク**：
  インターネット（TCP/IP）接続経路を匿名化するネットワーク。
- **Tor ソフトウェア**：
  Tor ネットワークの参加または使用に必要なソフトウェア。
- **Tor Project**：
  Tor のネットワークとソフトウェアを管理するアメリカの NPO 法人。

Tor は「オニオンルーティング」という暗号化方式を実装したもので、
トラフィックが Tor ノードを経由する度に暗号化・復号化が行われます。
玉ねぎの皮のように暗号化・復号化が行われることから
暗号化方式が「オニオンルーティング」と名付けられ、
名称の由来は最初の名称 "The Onion Router" の頭文字です。

世界中において、
約 6000--8000 の **Tor ノード**（中継点）[^Tor-nodes]と
約 250 万人の Tor 利用者が分散していて、
最も人気な匿名化ネットワークです。
使用するには登録や支払いが不要で、
基本的には誰でも無料で使用できます。
Tor ノードの立ち上げに関しても同様で、
ボランティアとして Tor ネットワークに貢献する制度です。

# 概要 {#overview}

## オニオンルーティング {#onion-routing}

<img class="display" src="../assets/img/Tor/onion-routing.png" width="685" height="286" alt="オニオンルーティング">

オニオンルーティングはネットワーク上の匿名化を実現します。
Tor クライアントはトラフィックを Tor ネットワークに中継させる前に、
自動的に無作為に 3 つのノードを選び[^guard-persistence]、
Tor ネットワークを通過する接続を成立させます。
選ばれた 3 つのノードで構成される接続は**回線**（circuit）と呼ばれます。
トラフィックは Tor 回線を経由してから接続先に到着します。

各ノードの呼称と意味は以下のとおりです。

- **ガードノード**：
  Tor ネットワークを通過するトラフィックがこのノードから入る、
  接続元側のノード。
- **中間ノード**：
  接続においてガードノードと出口ノードの間に位置するノード。
- **出口ノード**：
  Tor ネットワークを通過するトラフィックがこのノードから出る、
  接続先側のノード。

## ネットワーク通信の匿名化 {#network-anonymity}

<img class="display" src="../assets/img/Tor/network-anonymization.png" width="648" height="324" alt="ネットワーク通信の匿名化">

トラフィックが 3 つのノードを経由する
クリアネット[^clearnet]の接続の場合、
接続元、各ノードと接続先が得る接続情報
（例えば IP アドレス）は以下のとおりです。

| 観点 | 接続先 | ガードノード | 中間ノード | 出口ノード | 接続先 |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 接続元 | o   | o   | \*1  | \*1  | \*2  |
| ガードノード | o   | o   | o   |     |     |
| 中間ノード |     | o   | o   | o   |     |
| 出口ノード |     |     | o   | o   | o   |
| 接続先 |     |     |     | o   | o   |

- \*1：接続元が Tor 回線を無作為に選択するため既知。
- \*2：接続元が接続先への接続を要求するため既知。

要するに、接続元と接続先の両方が通信を行っている
という情報を得る単一の Tor ノードがありません。
また、接続先は接続元のネットワーク情報を得ず、
Tor からトラフィックが来たことしかわかりません。

従って、接続元のネットワーク通信の匿名化を実現します。

## 検閲と差別の回避 {#censorship-resistance}

Tor を利用することで接続元側と接続先側による検閲と差別を回避できます。

接続元側では、 ISP、カフェの Wi-Fi ハッカーまたはネットワーク管理者が、
地域やキーワードなどの接続先情報に基づいて
アクセスをブロックするかもしれません。
この場合に Tor を利用すると、
トラフィックが接続元側で暗号で保護され、
接続元側の攻撃者による接続先の地域や内容など
に基づく検閲が困難になります。

一方、接続先自身または接続先側にあるネットワーク管理者や ISP が、
接続元情報に基づいて接続拒否、情報提供の操作、
差別などをするかもしれません。
この場合に Tor を利用すると、
接続元のネットワーク情報が接続先側に届かず、
接続元側による接続元の地域や所属など
に基づく検閲と差別が困難になります。

ただし、 Tor 利用者全体に対する検閲と差別が簡単になってしまいます。
それを回避するには [Tor ブリッジ](#Tor-bridges)を検討してください。

## オニオンサービス {#onion-services}

インターネットサービスは通常
IP アドレスに結び付けられたドメイン名（例： `torproject.org`）で
クリアネット[^clearnet]上で運用するが、
IP アドレスを明かさないオニオンサービスで運用することができます。
**オニオンサービス**はオニオンアドレス（`.onion` 識別子のあるアドレス）
を持ち、 Tor でしか接続できないサービスです。

オニオンサービスへの接続（オニオン接続）では、
接続元および接続先が各自 Tor 回線を構築し、
中間で接続元の回路が接続先の回路に接続します。
接続に 6 つのノードが存在し、出口ノードが存在しません。

オニオンアドレスは、オニオンサービスを特定する
56 文字[^onion-address]と `.onion` で構成されます。
例えば、 Tor Project ホームページのオニオンアドレスは以下のとおりです。

`2gzyxa5ihm7nsggfxnu52rck2vv4rvmdlkiu3zzui5du4xyclen53wid.onion`

*注：上のアドレスの形式はバージョン 3 アドレスと呼ばれます。
旧式のバージョン 2 アドレス（例： `expyuzz4wqqyqhjn.onion`）は
2021 年に廃止されました。*

その結果、接続元のネットワーク上の匿名性だけでなく、
オニオンサービスを運用する接続先のネットワーク上の匿名性も保護します。
オニオンサービスは DNS や TLS などの弱点や問題を回避し、
独自に秘匿性や認証を実現します。
オニオンサービスは以下のセキュリティ特性を有します。

- エンドツーエンド認証：オニオンアドレスにオニオンサービスの身元公開鍵
  が入っているため、認証を実現。
- 強制的なエンドツーエンド暗号化（E2EE）での秘匿性の実現。
- 接続元と接続先の両方のネットワーク上の匿名性。
- ドメイン名と異なり、オニオンアドレスの登録と支払いは不要で、
  秘密にすることも可能。
- TLS と異なり、セキュリティ証明書の登録と支払いは不要。
- ほとんどそっくりの偽アドレスを用いた詐欺防止：
  既存オニオンサービスのアドレスに似ているアドレスの生成が困難。

詳しくは Tor Project のサイトを参照してください。

→ 外部資料：
`https://community.torproject.org/onion-services/overview/`

→ 外部資料：
`https://support.torproject.org/onionservices/`

技術仕様はこちら。

→ 外部資料：
`https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt`

# 注意点と限界 {#warnings-and-limitations}

Tor はネットワーク上の接続元と接続先との繋がり防止を実現するが、
プライバシーまたは匿名性を保証しません。
アプリケーションによる個人情報や IP アドレスなどの情報漏洩や
利用者自身の行動による情報漏洩の防止またはその情報の匿名化をせず、
Tor に対する攻撃も可能です。

本セクションでは、 Tor についての一部の注意点や限界について説明します。
以下に紹介する一部の点は Tor だけでなく
他の匿名化ネットワークやプライバシーツールにもあてはまります。

## Tor の匿名性集合 {#anonymity-set}

Tor の匿名性集合[^anonymity-set]は、ほとんどの場合、
同時に Tor を利用している人の集合より遥かに小さいです。
したがって、 Tor の価値には Tor 利用者数に依存する
ネットワーク効果があります。

ある行為について、 Tor が使われたという事実に加えて、
Tor 利用者の地域、使用言語、接続先、 OS、アプリケーション、
ブラウザ指紋、センシティブ情報の提供・漏洩、利用時間などの事実も知られると、
匿名性集合が段々小さくなります。
インターネットを直接的に利用したり VPN を利用したりなどに比べて、
自分の行為に対する匿名性集合が大きいだろうが、
Tor を利用しただけで匿名性が確保できると思わないでください。

## レイテンシ増大とスループット低下 {#latency-and-throughput}

Tor 回線は 3 つ（以上）の Tor ノードを経由してデータを転送するため、
直接接続よりレイテンシ（データ転送の待ち時間、ネットワーク遅延）
の増大があっても当然です。
多少のスループット（データ転送速度）の低下もあるかもしれません。

## TCP/IP 以外のトラフィックを中継できない {#TCP-IP-only}

Tor は TCP/IP トラフィックしか中継できません。
そのため、音声電話（VoIP）、ビデオ会議、ゲームなどの
UDP トラフィックを Tor で中継できません。

ちなみに、 TCP は接続を維持し、輻輳制御をしながら、
データ転送の信頼性と正確さを保証するプロトコルです。
一方、 UDP は無接続型で、データ転送の迅速さ（レイテンシ最小化）
を優先するプロトコルです。

## 出口ノードから通信を保護しない {#exit-node-problems}

オニオン接続以外、 Tor 自体は通信内容の秘匿性を提供しません。
接続元から出口ノードまでの接続は Tor のオニオンルーティングで保護されるが、
出口ノードから接続先までの接続は Tor で暗号化・認証されません。
出口ノードや接続先を監視するネットワーク管理者や
攻撃者または悪質な出口ノードが
そのトラフィックを監視・改竄・検閲・ロギングしたり
マルウェアを注入したりできるため要注意です。
通信内容を保護するには、接続先までの接続を保護する暗号
（例： TLS、オニオンサービス、 E2EE アプリケーション）が必要です。

## アプリケーションによる情報漏洩 {#leakage-by-applications}

あらゆるアプリケーションのトラフィックを
Tor 経由にすれば匿名性が得られるわけではありません。
多くのアプリケーションはセキュリティやプライバシーに全く配慮せず、
むしろ監視と情報収集のために開発された
アプリケーションは少なくありません。
利用者のセンシティブ情報（IP アドレス、個人情報など）を明かす
アプリケーションのトラフィックを Tor 経由にしても、
ほとんど意味がありません。

Tor は利用者のアプリケーション（クライアント）
に対する攻撃から保護しません。
接続元を特定する情報を抽出したり
追跡したりする接続先（例：ウェブサイト）は
クッキー（接続元で保存される情報）やブラウザ指紋抽出
（browser fingerprinting：クライアントや OS などに関する情報抽出）
などの手法で攻撃することが多いです。

アプリケーションによる情報漏洩のリスクを軽減するには、
以下の点を念頭に置いた方がいいでしょう。

- Tor を用いてダウンロードしたファイル（ドキュメント、画像など）
  を慎重に扱う。
  - インターネットに接続していないデバイスで開く。
  - インターネット接続の無いサンドボックスの中で開く。
  - サニタイズ[^Dangerzone]してから開く。
- セキュリティとプライバシーに十分配慮したアプリケーション
  だけによるネットワークアクセスを許可した上で Tor 経由にして、
  それ以外のアプリケーションによるネットワークアクセスを拒否・遮断する。

例えば、ウェブ閲覧をしたい場合、
Tor をプロキシとして設定されたどのウェブブラウザでも利用する代わりに
プライバシーと匿名性に特化した Tor Browser
を利用するといいかもしれません。
また、 BitTorrent は Tor とかなり衝突するため悪い組み合わせとなり、
Tor ネットワークに重い負荷をかけながら
BitTorrent 利用者に匿名性を提供しません。
BitTorrent の代わりに、例えば [OnionShare](#OnionShare) で
ファイルを共有した方がいいでしょう。

→ ガイド：
**Tor Browser**
--- [Markdown](../ja-md/alice2bob-ja-Tor-Browser-2.4.md)
\| [HTML](../ja-html/alice2bob-ja-Tor-Browser-2.4.html)

## 利用者の行動による情報漏洩 {#user-mistakes}

Tor は利用者の行動（利用パターンや情報提供など）による
情報漏洩を抑えようとしません。
利用者自身がセンシティブ情報を明かしたら、
トラフィックをどのように経由しても何も変わりません。
また、 Tor はフィッシングなどの詐欺から利用者を保護しません。

インターネット利用者が実践する良い習慣は重要な対策です。
ウェブ閲覧、ダウンロード、投稿、フォーム入力、ログインなどをする時に、
うっかり明かしてしまうかもしれない情報と
故意に提供する情報について考えた上で
インターネットを慎重に利用すると、このリスクを軽減できます。

## Tor を使用していることが明確 {#Tor-doesnt-hide-Tor-use}

通常の使い方のように Tor を使用すると、
Tor の使用が明らかになります。
Tor の使用はほとんどの管区では合法だろうが、
それでも隠した方がいい場合があります。
大学のネットワークから Tor を使用すると
大学のネットワーク管理者の注意を引いたり、
Tor 利用者がほとんどいない社会から Tor を使用すると
接続元側で目立つかもしれません。
Tor 利用者全体に対する検閲と差別も
簡単であることを覚えておくことが重要です。

例えば、 Tor 接続を拒否するウェブサイトが少なくありません。
また、サービス提供拒否、 CAPTCHA[^CAPTCHA] 要求などで
Tor 利用者を差別的に扱うサービスもあります。

技術的な解説をすると、 Tor ノードのリスト
（したがって IP アドレス情報）は公開されていて、
Tor ネットワーク内のトラフィックは
一般的には特定のポート番号を使用します。

Tor を使用していることを隠したい場合、
他のセキュリティ対策も同時に使う必要があります。
接続元側または接続先側で隠す方法はいくつかあります。
ただし、このような対策は[トラフィック指紋抽出](#traffic-fingerprinting)
からの保護を提供しないでしょう。

- [Tor ブリッジ](#Tor-bridges)
  （接続元側の Tor 使用を隠し、検閲回避する非公開ノード）
- [接続元側の VPN](#Tor-over-VPN)
  （Tor over VPN：接続元 → VPN → Tor → 接続先）
- [接続先側の VPN](#VPN-over-Tor)
  （VPN over Tor：接続元 → Tor → VPN → 接続先）

一方、接続先から Tor 使用を隠したり
Tor ブロッキングを回避したりしたい場合、
接続先側のプロキシ（例：ウェブアーカイブ、代替フロントエンド）
を利用すると効果的かもしれません。

→ ガイド：
**Tor Browser § Tor ブロッキングの回避**
--- [Markdown](../ja-md/alice2bob-ja-Tor-Browser-2.4.md#Tor-blocking-circumvention)
\| [HTML](../ja-html/alice2bob-ja-Tor-Browser-2.4.html#Tor-blocking-circumvention)

## Tor に対する攻撃 {#attacks-against-Tor}

[「オニオンルーティング」](#onion-routing)であげた情報に加えて、
各ノードと接続元と接続先ではトラフィックの特徴
（データ量、形状、タイミング）も存在します。
そのため、 Tor は完全な匿名化を実現せず、共謀する悪質の Tor ノード、
接続の両側でのロギングまたは監視、
インターネットを広範囲監視できる敵などから
保護してくれません。

Tor はトラフィックをできる限り速く中継しながら匿名化しようとする
**低レイテンシ**匿名化ネットワークです。
トラフィックを一時的に持ち続ける
高レイテンシ匿名化ネットワークと異なって、
強度なインターネット監視に対して保護することができません。

### トラフィック指紋抽出 {#traffic-fingerprinting}

<img class="display" src="../assets/img/Tor/traffic-fingerprinting.png" width="689" height="253" alt="トラフィック指紋抽出">

**トラフィック指紋抽出**（traffic fingerprinting）とは、
暗号化されたトラフィックを解読しようとせず、
インターネット利用者のトラフィックの特徴のみから、
利用者がアクセスしているサービス・内容を推測したり
Tor や VPN などの使用を推測したりする攻撃です。
暗号化されたトラフィックもデータ量、形状、タイミングなどの特徴を漏洩し、
サービスまたはウェブページの推測を可能にします。
Tor 使用の場合、利用者とガードノードの間のトラフィックを
監視・解析する形でこの攻撃が行われることが多いでしょう。
この攻撃は Tor だけでなく、
他の低レイテンシ匿名化ネットワークや VPN などに対しても効果的でしょう。

同じ Tor クライアントを用いて
同時に多数のサービス・ウェブページにアクセスすると、
トラフィックの混合によりこの攻撃を困難にすることができます。

→ 外部資料：
トラフィック指紋抽出攻撃の批判（2013）：
`https://blog.torproject.org/critique-website-traffic-fingerprinting-attacks/`

→ 外部資料：
トラフィック指紋抽出攻撃とその軽減（2019）：
`https://blog.torproject.org/new-low-cost-traffic-analysis-attacks-mitigations/`

### トラフィック相関攻撃 {#traffic-correlation-attack}

<img class="display" src="../assets/img/Tor/Sybil-attack.png" width="637" height="335" alt="悪質な Tor ノード支配者によるトラフィック相関攻撃">

**トラフィック相関攻撃**（traffic correlation attack）または
**エンドツーエンド相関攻撃**（end-to-end correlation attack）とは、
トラフィックを解読しようとせず、
対象者と接続先とのトラフィックを両側で観測・解析することで
対象者の身元を特定したり非匿名化したりする攻撃です。

主に資源・資金の多い攻撃者が行う攻撃だが、
トラフィック相関攻撃に協力する恐れがある
インターネット上の行為者といえば、
ローカルネットワーク管理者、 ISP、 VPN 提供者、
訪問先のサービスなど多数あります。

Tor ノードだけによる攻撃について、
一つのノード管理者が以下のいずれかを支配する場合、
トラフィック相関攻撃が可能です。

- オニオン接続以外における Tor 回線のガードノードと出口ノード
- オニオン接続における両側の Tor 回線のガードノード

### Sybil 攻撃 {#Sybil-attack}

**Sybil 攻撃**（Sybil attack）とは、
攻撃者が多数の仮名または匿名の身元を作成することで不当な影響力を入手し、
ネットワークの評判制度を転覆させる攻撃です。

Tor ノードを立ち上げるにはコンピュータのハードウェアと
インターネットアクセスの費用以外のコストが無く、
ノード管理者に対する厳格な身元確認や他のチェックも特にありません。
そのため、資源・資金の多い攻撃者は簡単に
多数の Tor ノードを立ち上げることができます。
一つの行為者が支配するノードの数が多くなるにつれて、
無作為に選ばれた Tor 回線に対する
[トラフィック相関攻撃](#traffic-correlation-attack)
が可能になる確率が上がります。

## 悪質または危殆化したデバイス {#bad-device}

Tor は悪質または危殆化したデバイス（OS、 BIOS、ハードウェアなど）
から利用者を保護できません。
攻撃者がマルウェア、キーロガー、悪質な BIOS、メモリの危殆化などで
デバイスを危殆化させた場合、
Tor はこのような攻撃から利用者を保護できません。

# アプリケーション {#applications}

Tor を利用する様々なアプリケーションがあります。

Tor Project が開発・管理しているか
公式に支持している一部のアプリケーションは以下です。

- **Onion Browser**：
  匿名性とプライバシーに特化したウェブブラウザ（iOS 用）。
- **Orbot**：
  オニオンルーティングを実装したクライアント（Android 用）。
- **Tor**：
  オニオンルーティングを実装したクライアント。
- **Tor Browser**：
  匿名性とプライバシーに特化したウェブブラウザ。
- **Torsocks**：
  他のアプリケーションのトラフィックを
  SOCKS を用いて Tor 経由にするラッパーコマンド。

一方、 Tor エコシステムを構成する一部の
第三者アプリケーションは以下です。

- **apt-transport-tor**：
  APT （Advanced Package Tool）のための
  パッケージの匿名ダウンロードのトランスポート。
- **Bisq**：
  非中央化の暗号通貨取引所。
  `https://bisq.network/`
- **Brave**：
  プライバシーと広告ブロッキングに特化したウェブブラウザ。
  `https://brave.com/`
- **F-Droid**：
  Android のソフトウェア管理。
  `https://f-droid.org/`
- **GlobaLeaks**：
  ウェブインターフェイスで実現した内部告発用情報提供システム。
  `https://globaleaks.org/`
- **OnionShare**：
  オニオンサービスを用いたファイル共有。
  `https://onionshare.org/`
- **parcimonie**：
  プライバシーを考慮した GnuPG 鍵輪更新ヘルパー。
  `https://salsa.debian.org/intrigeri/parcimonie/`
- **Qubes OS**：
  分離でのセキュリティ（security by isolation）
  に特化したデスクトップ OS。
  `https://qubes-os.org/`
- **SecureDrop**：
  報道機関が管理する内部告発用情報提供システム。
  `https://securedrop.org/`
- **Tails**：
  プライバシーと匿名性の保護に特化した持ち運び可能なデスクトップ OS。
  `https://tails.net/`
- 一部の暗号通貨ウォレット。

以下のセクションで一部のアプリケーションを説明します。
以下で説明するアプリケーション（Tor Browser 以外）を使うには、
Tor のインストールが必要です。

## 一般 Tor クライアント `tor` {#general-Tor-client}

Tor を利用する多くのアプリケーションは、
一般 Tor クライアント `tor` を要します。
例えば、 APT、 OnionShare、 Torsocks などには必要です。

一方、 Tails には事前に用意され、
Tor Browser には Tor クライアントが含まれます。
この 2 件には、一般 Tor クライアントのインストールが不要です。

本セクションでは Debian ベース OS での
APT を用いた一般 Tor クライアントの
インストール方法を説明します。

1. Tor Project の Debian リポジトリの公開鍵を APT に追加する。
2. Tor Project の Debian リポジトリを APT に追加する。
3. Tor をインストールする。

### Tor Project の Debian リポジトリの公開鍵 {#deb.torproject.org-keyring}

初めて Tor Project の Debian リポジトリを使用する際、
まずリポジトリの公開鍵を APT に追加する必要があります。

検証・インストールのために入手するファイルは以下です。
全てのパスは `https://deb.torproject.org/torproject.org`
または
`http://apow7mjfryruh65chtdydfmqfpj5btws7nbocgtaovhvezgccyjazpqd.onion/torproject.org`
内のものです。

- パッケージ： ...`/pool/main/d/deb.torproject.org-keyring/`
  内の最新の `.deb` ファイル
  （例： `deb.torproject.org-keyring_2020.11.18_all.deb`）
- パッケージ情報ファイル： ...`/dists/sid/main/binary-amd64/Packages`
- 署名されたリリース情報ファイル： ...`/dists/sid/InRelease`
- 公開鍵： ...`/` 内の `.asc` ファイル

検証・インストールの手順は以下です。
以下の署名または SHA256 の検証手順が失敗した場合、
入手したパッケージをインストールしないでください。
GnuPG、 SHA256 ハッシュ化と `.deb` パッケージのインストール方法
の説明をここで省略します。

1. 以上のファイルを入手する。
2. 公開鍵を GnuPG にインポートする。
3. リリース情報ファイルの署名を検証する。
4. パッケージ情報ファイルを SHA256 ハッシュ化して、
   リリース情報ファイル内の `main/binary-amd64/Packages`
   の SHA256 ハッシュ値と照合する。
5. パッケージファイルを SHA256 ハッシュ化して、
   パッケージ情報ファイル内のパッケージ `deb.torproject.org-keyring`
   に該当する SHA256 ハッシュ値と照合する。
6. パッケージをインストールする。

公開鍵のインストールが完了したら、
`/etc/apt/trusted.gpg.d/deb.torproject.org-keyring.gpg`
に公開鍵のファイルが現れます。

### APT への Tor Project の Debian リポジトリの追加 {#add-Debian-repository}

APT に Tor Project の Debian リポジトリを追加するには、
以下の内容のあるリポジトリ指定ファイル
`/etc/apt/sources.list.d/tor.list` を作成します。
利用している Debian ベース OS に該当するバージョンに
`<dist>` を置き換えてください。
（例： `sid`、 `bullseye`、 `xenial`）

~~~
deb [signed-by=/etc/apt/trusted.gpg.d/deb.torproject.org-keyring.gpg] https://deb.torproject.org/torproject.org/ <dist> main
~~~

### Tor のインストール {#install-tor}

Tor Project の Debian リポジトリの追加を完了した後、
以下のように Tor をインストールします。

~~~
# apt install tor
~~~

## apt-transport-tor {#apt-transport-tor}

**apt-transport-tor** とは、
APT のために開発されたリポジトリ情報と
パッケージの匿名ダウンロードのトランスポートです。

APT でパッケージを管理する OS を使っていて、
Debian リポジトリが APT に設定されている場合、
apt-transport-tor を以下のようにインストールします。

~~~
# apt install apt-transport-tor
~~~

その後、リポジトリ指定ファイル `/etc/apt/sources.list` と
`/etc/apt/sources.list.d/` を以下のように変更します。

- 各 `http://`... と `https://`... URL の先頭に `tor+` を追加する。
- リポジトリのオニオンサービスを利用したい場合、
  URL のドメイン部分を該当するオニオンアドレス
  のドメイン部分に置き換える。
  （オニオンサービスが HTTPS に対応していない限り、
  `https://` を `tor+http://` に置き換えます。）

例えば、 Tor Project の Debian リポジトリを指定するには、

~~~
deb [signed-by=/etc/apt/trusted.gpg.d/deb.torproject.org-keyring.gpg] https://deb.torproject.org/torproject.org/ sid main
~~~

を以下に置き換えます。

~~~
deb [signed-by=/etc/apt/trusted.gpg.d/deb.torproject.org-keyring.gpg] tor+http://apow7mjfryruh65chtdydfmqfpj5btws7nbocgtaovhvezgccyjazpqd.onion/torproject.org/ sid main
~~~

→ 外部資料：
`https://support.torproject.org/apt/#apt-3`

## OnionShare {#OnionShare}

**OnionShare** とは、
オニオンサービスを用いたファイル共有アプリケーションです。
以下の機能を持ちます。

- **ファイル共有**（Share Files）：
  共有者がオニオンサービスを起動させ、
  受信者が Tor Browser でファイルをダウンロードする。
- **ファイル受信**（Receive Files）：
  受信者がオニオンサービスを起動させ、
  送信者が Tor Browser でファイルを共有する。
- **ウェブサイトを発行する**（Host a Website）：
  発行者がオニオンサービスを起動させ、
  訪問者が Tor Browser でウェブサイトにアクセスする。
- **匿名でチャットする**（Chat Anonymously）：
  サービス提供者がオニオンサービスを起動させ、
  チャット参加者が Tor Browser でサービスにアクセスする。

インストールするには、以下のいずれかをします。

- `https://onionshare.org/dist/` から
  適切なインストールファイルと署名を
  ダウンロード・検証・インストールする。
- APT でパッケージ `onionshare` （Debian リポジトリ）
  をインストールする。

→ 外部資料：
`https://docs.onionshare.org/`

## Tor Browser {#Tor-Browser}

Tor Browser を別のガイドで説明します。

→ ガイド：
**Tor Browser**
--- [Markdown](../ja-md/alice2bob-ja-Tor-Browser-2.4.md)
\| [HTML](../ja-html/alice2bob-ja-Tor-Browser-2.4.html)

## Torsocks {#Torsocks}

**Torsocks** とは、
他のアプリケーションのトラフィックを SOCKS を用いて
Tor ネットワーク経由にするラッパーコマンドです。
SOCKS に対応するアプリケーションを Tor 化してくれるコマンドです。
DNS 要求を安全に扱い、 TCP 以外のトラフィックを遮断します。

APT でパッケージを管理する OS を使っていて、
Debian リポジトリが APT に設定されている場合、
Torsocks を以下のようにインストールします。

~~~
# apt install torsocks
~~~

使い方は Tor 化したいコマンドの先頭に `torsocks` を追加するだけです。

~~~
$ torsocks <application>
~~~

例えば、 SSH を Tor 化するには、以下のように使います。

~~~
$ torsocks ssh user@host
~~~

*注： Torsocks は匿名性とアプリケーションの通信内容の秘匿化
を[保証しません](#warnings-and-limitations)。
利用者のセンシティブ情報を漏洩するアプリケーションがあり、
そのような場合に Torsocks をかけても匿名性または秘匿性を得ません。*

→ 外部資料：
`https://gitlab.torproject.org/tpo/core/torsocks/`

→ 外部資料：
`https://gitlab.torproject.org/legacy/trac/-/wikis/doc/torsocks/`

# Tor ブリッジ {#Tor-bridges}

**Tor ブリッジ**（Tor bridge）とは、
Tor 利用者の Tor ネットワークへの接続を支援する非公開 Tor ノードです。
公開 Tor ノードと同様に誰でもブリッジを立ち上げることができます。
Tor ブリッジは Tor ノードだとバレない限り、
公開 Tor ノードのブロッキングを回避する方法を提供します。
Tor または VPN がブロックされているかその使用が目立つ状況においても、
ブリッジを用いて Tor に接続するといいかもしれません。

Tor ブリッジは公開ノードではなく、
ブロッキング回避や Tor の使用の隠しに利用できるが、
無期限に Tor の使用を隠してくれる保護はありません。
何かしらの方法によって Tor ブリッジが Tor ノードだとバレたら、
ISP またはローカルネットワークに Tor の使用もバレてしまいます。
Tor の使用を ISP またはローカルネットワークから隠したい場合、
VPN が使える場合、 [VPN との併用](#Tor-and-VPN-together)を推奨します。

→ 外部資料：
Tor ブリッジ：
`https://tb-manual.torproject.org/bridges/`

→ 外部資料：
検閲回避：
`https://tb-manual.torproject.org/circumvention/`

## Pluggable transport {#pluggable-transports}

公開 Tor ノードと異なって、
ブリッジは接続が Tor 接続であることを隠そうとする
pluggable transport を利用します。
Pluggable transport はある種のトラフィックに見せかけることで、
ブリッジをブロッキングから保護します。
以下の種類の pluggable transport を用いるブリッジが
Tor Browser に内蔵されています。

- **obfs4**：トラフィックを無作為のように見せかける。
- **meek**：大手のウェブサイトに接続しているように見せかける。
- **snowflake**： WebRTC ピアツーピアプロトコルのように見せかける。

→ 外部資料：
Snowflake：
`https://snowflake.torproject.org/`

## ブリッジの入手 {#get-bridges}

ブリッジを入手するには、以下の方法があります。

- Tor Project のブリッジ用サイトに訪問してブリッジを要求する。
  `https://bridges.torproject.org/`
- メールアドレス `bridges@torproject.org` 宛に
  希望のブリッジ種類を本文に書かれたメール（空白の件名）を送る。
  （例： "get transport obfs4"）
  （`riseup.net` または `gmail.com`
  のメールアドレス以外から利用できません。）
- Tor Browser の内蔵ブリッジを選択する。（Tor Browser 専用）
- Tor Browser のインターフェイスを通じて
  Tor Project からブリッジを要求する。

入手方法によって CAPTCHA[^CAPTCHA] が要求されるかもしれません。

→ ガイド：
**Tor Browser § Tor ブリッジの設定**
--- [Markdown](../ja-md/alice2bob-ja-Tor-Browser-2.4.md#configure-Tor-bridge)
\| [HTML](../ja-html/alice2bob-ja-Tor-Browser-2.4.html#configure-Tor-bridge)

## ブリッジの追加 {#add-bridges}

Pluggable transport の種類や
IP プロトコルバージョン（IPv4 または IPv6）によって、
以下のような形式でブリッジを入手するでしょう。

Pluggable transport なしの IPv4 ブリッジの例（2 つ）。

~~~
xxx.xxx.xx.xx:443 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
xxx.xxx.xxx.xxx:9001 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
~~~

obfs4 の IPv4 ブリッジの例（3 つ）。

~~~
obfs4 xxx.xxx.xxx.xx:990 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX cert=YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY iat-mode=0
obfs4 xx.xx.xxx.xx:8080 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX cert=YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY iat-mode=0
obfs4 xx.xx.xxx.xxx:46089 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX cert=YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY iat-mode=0
~~~

Pluggable transport なしの IPv6 ブリッジの例（1 つ）。

~~~
[xxxx:xxxx:xx:xxx::x]:9001 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
~~~

### `tor` へのブリッジ追加 {#add-bridges-to-Tor}

一部のアプリケーション（例えば Tor Browser や OnionShare）は
個別のインターフェイスを提供するため簡単です。
一方、一般 Tor クライアント `tor` にブリッジを使用させるには、
設定ファイルを編集する必要があります。

obfs4 ブリッジを使うには、
`obfs4proxy` をインストールする必要があるかもしれません。
以下のように Debian リポジトリから APT を用いてインストールします。

~~~
# apt install obfs4proxy
~~~

ブリッジを利用するには、ブリッジを有効化して、
必要に応じて pluggable transport の設定をします。
設定ファイル（Debian ベース OS： `/etc/tor/torrc`）
を編集することで設定を行います。
設定ファイルを編集するにはルート権限が必要でしょう。

~~~
UseBridges 1
ClientTransportPlugin obfs4 exec /usr/bin/obfs4proxy
~~~

そして、ブリッジ `<bridge>` を一行ずつ設定ファイルに追加します。

~~~
Bridge <bridge>
...
~~~

例えば、ブリッジと obfs4 を有効化し、
1 つの obfs4 ブリッジを追加したい場合に追加する行です。

~~~
UseBridges 1
ClientTransportPlugin obfs4 exec /usr/bin/obfs4proxy
Bridge obfs4 xxx.xxx.xxx.xx:990 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX cert=YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY iat-mode=0
~~~

最後に、 `tor` を（再）起動させることでブリッジを適用します。

~~~
# service tor start
~~~

# Tor と VPN {#Tor-and-VPN}

本セクションでは、 VPN について説明して、
Tor と VPN の併用について説明します。

→ 外部資料：
`https://gitlab.torproject.org/legacy/trac/-/wikis/doc/TorPlusVPN`

## VPN {#VPN}

**VPN** （Virtual Private Network）とは、
公的ネットワーク（例：インターネット）に跨って、
ある私的（プライベート）ネットワークを拡張する技術です。
VPN は、 VPN 利用者と VPN 提供者との間で、
ネットワーク通信の認証、完全性と秘匿性を提供します。
複数の拠点をわたってあるネットワーク（例：企業のネットワーク）を
安全に拡張することに多く使われます。

インターネット監視・検閲を回避する目的で VPN を使う人は多く、
セキュリティとプライバシーを重視するコミュニティに
広く知られている技術です。
しかし、インターネット監視を回避する目的とした VPN 応用について、
残念ながら勘違いが多いみたいです。
以下のセクションで VPN のメリットとデメリットについて簡潔に説明します。

### インターネット利用者側の監視・検閲の回避 {#local-surveillance-censorship}

インターネット利用者側の監視・検閲に対して、
VPN はそれの防止・軽減にある程度の効果があります。

インターネット利用者側の ISP は全てのトラフィックをロギングしたり
特定のトラフィックをブロックしたりすることが多いです。
また、ローカルネットワークの管理者または公共 Wi-Fi の他の利用者は
ネットワーク利用者のトラフィックを監視することがあります。

このような事例では、インターネット利用者は VPN を利用することで、
トラフィックの通信情報と通信内容の秘匿性と完全性を保護できます。
（ただし、[トラフィック指紋抽出](#traffic-fingerprinting)から
保護しません。）
国や ISP や現地の状況などによって、
このように VPN （またはプロキシ）を用いて
インターネットにアクセスせざるを得ない人たちがいます。

### 接続先側のブロッキングの回避 {#server-side-blocking}

一方、 VPN を用いて接続先側のブロッキングを回避できるかもしれません。
例えば、インターネットサービスまたは接続先側の ISP は、
Tor ネットワークとの接続を拒否したり
特定の国（またはそれ以外）からの接続を拒否したりする場合、
VPN を用いて接続を達成できる場合があります。

### VPN の匿名性と秘匿性は限定的 {#VPN-limited-anonymity-confidentiality}

VPN に関する代表的な勘違いといえば、
VPN が匿名性を提供するという勘違いです。
接続元は VPN を利用することで
接続先（例：ウェブサイト）から自分の IP アドレスを隠すことができるが、
VPN 提供者は以下の情報を得て、たいていはロギング・記録をします。
その上、 VPN 提供者は VPN サーバで出入りするトラフィックを盗聴できます。
そのため、 VPN 提供者または VPN 技術を危殆化させるだけで、
VPN 利用者を攻撃できます。

- VPN 利用者の登録情報（氏名、支払い情報など）
- VPN 利用者の IP アドレス、したがって地域
- VPN トラフィックの通信情報（接続先、ポート番号、時間帯、
  形状、データ量など）
- 非暗号化通信内容（例：暗号化を用いない HTTP）を含む
  VPN 利用者のトラフィック

匿名性を希望する場合、
VPN （だけ）でなく匿名化ネットワークを使うことを推奨します。

### VPN 関連の他の注意点 {#other-VPN-warnings}

VPN の登録と使用について、他に複数の問題があります。

- 多くの VPN 提供者が「ロギングをしません」と広告するが、
  その主張を検証不可能なため、
  ロギングが行われていると仮定するしかありません。
- VPN 提供をビジネスの事業として行われていることが多いため、
  たいていは登録・支払いをする必要があって、
  それによって匿名性が失われます。
- インターネットを使う前に利用者が
  VPN 接続を成立させることを忘れることがあって、
  うっかり VPN を経由せずにトラフィックを送受信する恐れがあります。
- インターネットアクセスの不良または VPN 提供者側の問題によって
  VPN 接続が突然落ちることが多く、
  VPN が突然落ちた直後の全てのトラフィックが
  VPN 経由せずに送受信されてしまいます。
  これを未然防止するには、 VPN が落ちた瞬間に全てのトラフィックを
  遮断する効果的な「キルスィッチ」（kill switch）の設置が必要です。
- 当然ながら VPN 提供者は VPN 利用者の IP アドレスを得るため、
  インターネットを利用する地域も把握できます。
  動き回りながら VPN を引き続き利用した場合、
  VPN 提供者が移動を追跡できるでしょう。

## Tor と VPN の併用 {#Tor-and-VPN-together}

Tor と VPN を併用することで、
両者の一部のメリットを得ながら両者の一部のデメリットを削減できます。
しかし、技術を組み合わせて併用する前に、
組み合わせ全体のセキュリティ特性を理解・検討する必要があります。

Tor と VPN を併用する場合、以下のみを一般的に推奨します。

- 接続元 → VPN → Tor → 接続先

一方、以下を一般的に推奨しません。

- 接続元 → Tor → VPN → 接続先
- 接続元 → VPN → Tor → VPN → 接続先
- その他の Tor と VPN の併用方法

→ 外部資料：
Tor と VPN の併用の推奨：
`https://discuss.privacyguides.net/t/clarify-tors-weaknesses-with-respect-to-observability/3676/4`

### 接続元 → VPN → Tor → 接続先 {#Tor-over-VPN}

**Tor over VPN** （接続元 → VPN → Tor → 接続先）とは、
VPN 接続を経由して Tor に接続する接続方法です。

ISP またはローカルネットワークを信用しない場合、
この方法で接続することにはメリットがあります。
この方法では、利用者が Tor を使用していることが
利用者側から隠されるかもしれません。
（ただし、[トラフィック指紋抽出](#traffic-fingerprinting)から
保護しません。）
状況によって VPN も疑われるかもしれないが、
一般的には VPN は Tor より多く使われているため、
この方法は Tor だけより目立たないかもしれません。

Tor に対する攻撃が成功しても、 VPN 経由で Tor に接続するため、
VPN のおかげで攻撃から利用者の IP アドレスが保護されるかもしれません。
一方、 VPN に対する攻撃が成功しても、
VPN 利用者のトラフィックは Tor 経由のため、
接続先情報や通信内容が保護されるかもしれません。

VPN の代わりに [Tor ブリッジ](#Tor-bridges)を利用すると、
多少似ている特性が得られるでしょう。
ただし、 Tor ブリッジは Tor の使用を無期限に隠しません。
Tor の使用を ISP またはローカルネットワークから隠したい場合、
この VPN との併用をした方がいいかもしれません。

### 接続元 → Tor → VPN → 接続先 {#VPN-over-Tor}

**VPN over Tor** （接続元 → Tor → VPN → 接続先）とは、
Tor を経由して VPN に接続する接続方法です。

この接続方法では、接続先側から見て
VPN のサーバから接続しているように見えます。
接続先側による Tor ブロッキングを回避するために
この接続方法を使う人がいます。

しかし、このようにインターネットを利用したら、
接続先側から見て VPN 利用者の匿名性は
VPN だけを利用する場合と同じ程度で、
匿名ではありません。

その上、 Tor は通常、 Tor 回線を頻繁に変更するが、
引き続き VPN に接続していると Tor 回線の変更が少なくなって、
同じ Tor 回線を長時間使用してしまいます。

接続元側では、 ISP やローカルネットワークは
利用者が Tor を利用していることが簡単にわかります。

# おまけ：他の匿名化ネットワーク {#other-anonymization-networks}

以下のネットワークには、
様々な手段での通信（TCP、 UDP など）への対応、
ピアツーピア通信、 Sybil 攻撃耐性など、
Tor には無いまたは Tor の弱点を改善する機能・特徴があります。

- Freenet： `https://freenetproject.org/`
- GNUnet： `https://gnunet.org/`
- I2P： `https://geti2p.net/`
- Lokinet： `https://lokinet.org/`
- Nym： `https://nymtech.net/`
- リメーラ（remailer）：匿名メールを転送するネットワーク



[^anonymity-set]: 匿名性集合（anonymity set）とは、
ある行為をしたことがあり得る匿名候補者の集合のことです。
匿名性集合に含まれる人数が多いほど匿名性が強いです。
その行為についての事実が知られることが多くなるにつれて、
候補者が絞られ、匿名性が低下します。

[^CAPTCHA]: CAPTCHA （キャプチャ）とは、
応答者がコンピュータでなく人間であることを確認するための
チャレンジレスポンス認証です。
変形した英数字の認識、写真に写っている物体の認識、音声認識など
コンピュータにとって困難な作業が認証の主な形式です。

[^clearnet]: クリアネット（clearnet）：
暗号化が任意な、公がアクセスできる通常のインターネット。
また、ダークネットまたは匿名化ネットワークの範囲内から出ること。

[^Dangerzone]: 例えば、 Dangerzone で
サニタイズするといいかもしれません。
`https://dangerzone.rocks/`

[^guard-persistence]: ただし、攻撃者が
ガードノードと出口ノードを支配する場合、
匿名性が破れてしまいます。
そのため、 Tor は一定の期間（数ヶ月）
同じガードノードを使い続けることで、
その攻撃の可能性を低下させようとします。

[^onion-address]: オニオンアドレスの 56 文字はオニオンサービスを特定する
身元公開鍵（256 ビット）、チェックサム（16 ビット）と
バージョンバイト（8 ビット）（計 280 ビット）で構成され、
小文字 `a-z` と数字 `2-7` を用いた 32 進数で符号化した文字列です。

[^Tor-nodes]: ただし、主にアメリカとドイツに集中していると見られます。


***

# アリスとボブ {#back}

ガイド一覧に戻る
--- [Markdown](../ja-md/alice2bob-ja-preface-2.4.md#list)
\| [HTML](../ja-html/alice2bob-ja-preface-2.4.html#list)

## 連絡先 {#back-contact}

### PGP 鍵 {#back-contact-PGP}

`1D3E 313C 4DB2 B3E0 8271 FC48 89BB 4CBB 9DBE 7F6D`

- ウェブサイト上：
  `https://git.disroot.org/alice2bob/alice2bob/src/branch/master/alice2bob.asc`
- 鍵サーバ（クリアネット）：
  `https://keys.openpgp.org/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D`
- 鍵サーバ（オニオン）：
  `http://zkaan2xfbuxia2wpf7ofnkbz6r5zdbbvxbunvp5g2iebopbfc4iqmbad.onion/search?q=1D3E313C4DB2B3E08271FC4889BB4CBB9DBE7F6D`

### ウェブサイト {#back-contact-website}

<img class="qr" src="../assets/qr/contact-website.png" width="222" height="222" alt="ウェブサイト QR コード">

`https://git.disroot.org/alice2bob/alice2bob`

## 検証 {#back-verify}

`https://git.disroot.org/alice2bob/alice2bob/src/branch/master/verify.md`

## 免責事項 {#back-disclaimer}

アリスとボブはセキュリティ専門家でも法律家でも
金融アドバイザーでも医師でもありません。
本シリーズの目的はあくまでも情報提供で、何の助言でもありません。
本シリーズは全くの無保証で提供されます。
本シリーズによってどんな不利、損失または損害が生じても、
アリスとボブは一切の責任を負いません。
ご自身の状況を考慮し、自己責任で本シリーズを使ってください。
